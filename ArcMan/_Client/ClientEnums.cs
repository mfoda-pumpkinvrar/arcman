﻿using System;

namespace ArcMan
{
    public enum ClientMessageType
    {
        Join = 0,
        StateUpdate,
        Exit,
        ConfigCorrupted,
        ConfigMissing,
        IOFailure,
        Exception,
    }

    public enum ClientState
    {
        Disconnected = 0,
        VR_Preparing,
        VR_Ready,
        Game_Preparing,
        Game_Ready,
        Game_Playing,
        Error
    }

    public static class ClientStateExt
    {
        public static StatusColor GetColor(this ClientState state)
        {
            switch (state)
            {
                case ClientState.Disconnected:
                    return StatusColor.Black;

                case ClientState.VR_Preparing:
                case ClientState.Game_Preparing:
                    return StatusColor.Yellow;

                case ClientState.VR_Ready:
                    return StatusColor.Blue;

                case ClientState.Game_Ready:
                    return StatusColor.Green;

                case ClientState.Game_Playing:
                    return StatusColor.Green_Play;

                case ClientState.Error:
                    return StatusColor.Red;
            }
            throw new System.Exception("Client was not in known state");
        }
        public static bool Equals(ClientState s1, ClientState s2) => s1 == s2;
        public static bool EqualsOr(ClientState s1, ClientState s2, ClientState s3) => s1 == s2 || s1 == s3;
        public static bool NotEqualsBoth(ClientState s1, ClientState s2, ClientState s3) => s1 != s2 && s1 != s3;
    }

    public enum StatusColor
    {
        Black = 0,
        Yellow,
        Blue,
        Green,
        Green_Play,
        Red
    }

    [Flags]
    public enum ClientErrorType
    {
        None                    = 0,
        SessionConnectionFailed = 1,
        ClientStoppedResponding = 2,
        VRLaunchFailed          = 4,
        GameLaunchFailed        = 8,
        TabooSceneOutOfSync     = 16,
        StarVR_Malfunctioned    = 32,
        StarVR_Frozen           = 64,
    }
}
