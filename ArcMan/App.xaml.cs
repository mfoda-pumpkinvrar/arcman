﻿using System;
using System.Text.RegularExpressions;

using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.ApplicationModel.ExtendedExecution.Foreground;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace ArcMan
{
    public sealed partial class App : Application
    {
        public static SessionManager SessionManager { get; } = new SessionManager();
        public static string ActivationPath;
        public static Client Client;
        public static bool IsAdmin = false;

        public App()
        {
            this.Suspending += OnSuspending;
            this.UnhandledException += App_UnhandledException;
            RequestExtendedExecution();
            LoadErrorConfig();
        }

        private async void App_UnhandledException(object sender, Windows.UI.Xaml.UnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            var f = await ApplicationData.Current.LocalFolder.CreateFileAsync("log.txt", CreationCollisionOption.OpenIfExists);
            var exception = e.Exception;
            await FileIO.AppendTextAsync(f, $"Unhandled exception - {exception}");

            if (!IsAdmin)
            {
                Client.ExceptionMessage = exception.Message;
                await Client.SendMessageToHostAsync(ClientMessageType.Exception);
            }

            var launchArg = App.IsAdmin ? "Admin" : $"PC{App.Client.DeviceIndex}";
            await CoreApplication.RequestRestartAsync(launchArg);
        }

        private async void LoadErrorConfig()
        {
            var file = await StorageFile.GetFileFromPathAsync(Constants.APP_ERROR_CONFIG);
            var txt = await FileIO.ReadTextAsync(file);
            string admin_connection, client_connection, vr_start, game_start;
            try
            {
                admin_connection = Regex.Match(txt, @"Admin\s+Connection\s+Timeout\s+=\s+(\d+)", RegexOptions.IgnoreCase).Groups[1].Value;
            }
            catch (Exception)
            {
                admin_connection = Constants.TIMEOUT_ADMIN_CONNECTION.ToString();
            }
            try
            {
                client_connection = Regex.Match(txt, @"Client\s+Connection\s+Timeout\s+=\s+(\d+)", RegexOptions.IgnoreCase).Groups[1].Value;
            }
            catch (Exception)
            {
                client_connection = Constants.TIMEOUT_CLIENT_CONNECTION.ToString();
            }
            try
            {
                vr_start = Regex.Match(txt, @"VR\s+Start\s+Timeout\s+=\s+(\d+)", RegexOptions.IgnoreCase).Groups[1].Value;
            }
            catch (Exception)
            {
                vr_start = Constants.TIMEOUT_VR_START.ToString();
            }
            try
            {
                game_start = Regex.Match(txt, @"Game\s+Start\s+Timeout\s+=\s+(\d+)", RegexOptions.IgnoreCase).Groups[1].Value;
            }
            catch (Exception)
            {
                game_start = Constants.TIMEOUT_GAME_START.ToString();
            }

            Constants.TIMEOUT_ADMIN_CONNECTION = int.Parse(admin_connection);
            Constants.TIMEOUT_CLIENT_CONNECTION = int.Parse(client_connection);
            Constants.TIMEOUT_VR_START = int.Parse(vr_start);
            Constants.TIMEOUT_GAME_START = int.Parse(game_start);
        }

        private async void RequestExtendedExecution()
        {
            //await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("Cleanup");
            await ExtendedExecutionForegroundHelper.RequestSessionAsync(
                    ExtendedExecutionForegroundReason.Unconstrained, null,
                    $"ArcMan ${(IsAdmin ? "Admin" : "Client")} Process"
                );
        }

        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            ExtendedExecutionForegroundHelper.ClearSession();
            if (!IsAdmin)
                await App.Client.SendMessageToHostAsync(ClientMessageType.Exit);

            App.SessionManager.EndSession();
            
            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("Cleanup");
            
            deferral.Complete();
        }

        protected override void OnActivated(IActivatedEventArgs args)
        {
            base.OnActivated(args);

            if (args.Kind == ActivationKind.CommandLineLaunch)
            {
                var cmdLineArgs = args as CommandLineActivatedEventArgs;
                var operation = cmdLineArgs.Operation;
                var cmdLineString = operation.Arguments;
                ActivationPath = operation.CurrentDirectoryPath;
                IsAdmin = cmdLineString.Contains("Admin");

                if (!IsAdmin)
                {
                    Client = new Client { ShouldMonitorErrors = true };
                    if (cmdLineString.Contains("PC1"))
                        Client.DeviceIndex = 1;
                    if (cmdLineString.Contains("PC2"))
                        Client.DeviceIndex = 2;
                    if (cmdLineString.Contains("PC3"))
                        Client.DeviceIndex = 3;
                    if (cmdLineString.Contains("PC4"))
                        Client.DeviceIndex = 4;
                }
            }

            if (!(Window.Current.Content is Frame rootFrame))
            {
                rootFrame = new Frame();
                Window.Current.Content = rootFrame;

                var viewType = IsAdmin ? typeof(AdminView) : typeof(ClientView);
                rootFrame.Navigate(viewType, args);
            }
            Window.Current.Activate();
        }

        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            // Only command-line launch and Restart are allowed since we use args to determine client/server role
            var args = e.Arguments;
            if (string.IsNullOrEmpty(args))
                App.Current.Exit();

            IsAdmin = args.Contains("Admin");

            if (!IsAdmin)
            {
                Client = new Client { ShouldMonitorErrors = true };
                if (args.Contains("PC1"))
                    Client.DeviceIndex = 1;
                if (args.Contains("PC2"))
                    Client.DeviceIndex = 2;
                if (args.Contains("PC3"))
                    Client.DeviceIndex = 3;
                if (args.Contains("PC4"))
                    Client.DeviceIndex = 4;
            }

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (!(Window.Current.Content is Frame rootFrame))
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }
                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    // When the navigation stack isn't restored navigate to the first page,
                    // configuring the new page by passing required information as a navigation
                    // parameter
                    var viewType = IsAdmin ? typeof(AdminView) : typeof(ClientView);
                    rootFrame.Navigate(viewType, e.Arguments);
                }
                // Ensure the current window is active
                Window.Current.Activate();
            }
        }

        private void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }
    }
}
