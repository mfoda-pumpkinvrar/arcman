﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ArcMan
{
    public static class Utils
    {
        /// <summary>
        /// Gets most used Mac address (as opposed to first)
        /// https://stackoverflow.com/a/51821927
        /// </summary>
        /// <returns></returns>
        public static string GetDefaultMacAddress()
        {
            Dictionary<string, long> macAddresses = new Dictionary<string, long>();
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses[nic.GetPhysicalAddress().ToString()] = nic.GetIPStatistics().BytesSent + nic.GetIPStatistics().BytesReceived;
                }
            }
            long maxValue = 0;
            string mac = "";
            foreach (KeyValuePair<string, long> pair in macAddresses)
            {
                if (pair.Value > maxValue)
                {
                    mac = pair.Key;
                    maxValue = pair.Value;
                }
            }
            return mac;
        }

        public static IPAddress GetLocalIPAddress()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }

        public static Task WakeOnLan(string macAddress)
        {
            return Task.Run(() =>
            {
                macAddress = Regex.Replace(macAddress, "[-|:]", "");       // Remove any semicolons or minus characters present in our MAC address

                var sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
                {
                    EnableBroadcast = true
                };

                int payloadIndex = 0;

                /* The magic packet is a broadcast frame containing anywhere within its payload 6 bytes of all 255 (FF FF FF FF FF FF in hexadecimal), followed by sixteen repetitions of the target computer's 48-bit MAC address, for a total of 102 bytes. */
                byte[] payload = new byte[1024];    // Our packet that we will be broadcasting

                // Add 6 bytes with value 255 (FF) in our payload
                for (int i = 0; i < 6; i++)
                {
                    payload[payloadIndex] = 255;
                    payloadIndex++;
                }

                // Repeat the device MAC address sixteen times
                for (int j = 0; j < 16; j++)
                {
                    for (int k = 0; k < macAddress.Length; k += 2)
                    {
                        var s = macAddress.Substring(k, 2);
                        payload[payloadIndex] = byte.Parse(s, NumberStyles.HexNumber);
                        payloadIndex++;
                    }
                }

                sock.SendTo(payload, new IPEndPoint(IPAddress.Broadcast, 0));  // Broadcast our packet
                sock.Close(1000);
            });
        }

        /// <summary>
        /// Sends a Wake-On-Lan magic packet to the specified MAC address.
        ///  The magic packet is a very simple fixed size frame that consists of 6 bytes of ones (FF FF FF FF FF FF) followed by sixteen repetitions of the target MAC address, and is sent as a broadcast UDP packet
        /// </summary>
        /// <param name="macAddress">Physical MAC address to send WOL packet to.</param>
        //public static void WakeOnLan(byte[] macAddress)
        //{
        //    // WOL packet contains a 6-bytes trailer and 16 times a 6-bytes sequence containing the MAC address.
        //    byte[] packet = new byte[17 * 6];

        //    // Trailer of 6 times 0xFF.
        //    for (int i = 0; i < 6; i++)
        //    {
        //        packet[i] = 0xFF;
        //    }

        //    // Body of magic packet contains 16 times the MAC address.
        //    for (int i = 1; i <= 16; i++)
        //    {
        //        for (int j = 0; j < 6; j++)
        //        {
        //            packet[i * 6 + j] = macAddress[j];
        //        }
        //    }

        //    // Send WOL packet over UDP 255.255.255.0:0.
        //    UdpClient client = new UdpClient();
        //    client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
        //    client.EnableBroadcast = true;
        //    //client.Connect(IPAddress.Broadcast, 0);
        //    client.Send(packet, packet.Length, new IPEndPoint(IPAddress.Broadcast, 0x1));
        //}
    }
}
