@echo off

echo Uninstalling. Please wait...

set STARTUP="%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup"
set DESKTOP="%USERPROFILE%\Desktop"

del /F /Q %STARTUP%\Admin.exe > nul 2>&1
del /F /Q %STARTUP%\PC1.exe > nul 2>&1
del /F /Q %STARTUP%\PC2.exe > nul 2>&1
del /F /Q %STARTUP%\PC3.exe > nul 2>&1
del /F /Q %STARTUP%\PC4.exe > nul 2>&1

del /F /Q %DESKTOP%\Admin.exe > nul 2>&1
del /F /Q %DESKTOP%\PC1.exe > nul 2>&1
del /F /Q %DESKTOP%\PC2.exe > nul 2>&1
del /F /Q %DESKTOP%\PC3.exe > nul 2>&1
del /F /Q %DESKTOP%\PC4.exe > nul 2>&1

Powershell.exe -Command "& Remove-ProvisioningPackage -PackagePath '../Installer/Step1/InstallArcMan.ppkg'" 
Powershell.exe -Command "& Get-AppxPackage 'PumpkinStudio.ArcMan' | Remove-AppxPackage"