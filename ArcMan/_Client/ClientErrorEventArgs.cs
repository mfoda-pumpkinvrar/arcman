﻿using System;

namespace ArcMan
{
    public class ClientErrorEventArgs : EventArgs
    {
        public ClientState StartState { get; set; }
        public ClientState TargetState { get; set; }
        public ClientErrorType ErrorType { get; private set; }

        public ClientErrorEventArgs(ClientState _startState, ClientState _targetState)
        {
            StartState = _startState;
            TargetState = _targetState;

            switch (StartState)
            {
                case ClientState.Disconnected:
                    ErrorType = ClientErrorType.SessionConnectionFailed;
                    break;
                case ClientState.VR_Preparing:
                    ErrorType = ClientErrorType.VRLaunchFailed;
                    break;
                case ClientState.Game_Preparing:
                    ErrorType = ClientErrorType.GameLaunchFailed;
                    break;
                case ClientState.Game_Playing:
                    ErrorType = ClientErrorType.TabooSceneOutOfSync;
                    break;
            }
        }
    }
}
