﻿using System;

namespace ArcMan
{

    public enum DisplayLanguage
    {
        JA_JP = 0,
        ZH_CN,
        KO_KR,
        EN_US,
        NONE,
    }

}
