﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace ArcMan
{
    [DataContract]
    public class AdminMessage
    {
        [DataMember]
        public AdminMessageType MessageType { get; set; }
        [DataMember]
        public DisplayLanguage PC1Language { get; set; }
        [DataMember]
        public DisplayLanguage PC2Language { get; set; }
        [DataMember]
        public DisplayLanguage PC3Language { get; set; }
        [DataMember]
        public DisplayLanguage PC4Language { get; set; }
        [DataMember]
        public int GameHostIndex { get; set; }
        [DataMember]
        public int VideoPlayIndex { get; set; }
        [DataMember]
        public string ChatHostIP { get; set; }
        [DataMember]
        public int GameStep { get; set; }
        [DataMember]
        public int ShutDownDeviceIndex { get; set; }
        [DataMember]
        public int CloseTabooDeviceIndex { get; set; }

        public override string ToString()
        {
            var str = "";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this))
                str += $"{descriptor.Name}: {descriptor.GetValue(this)}\n";

            return str;
        }

        public string ToJsonString()
        {
            var ser = new DataContractJsonSerializer(typeof(AdminMessage));
            var ms = new MemoryStream();
            ser.WriteObject(ms, this);
            byte[] json = ms.ToArray();
            ms.Close();
            var rawJson = Encoding.UTF8.GetString(json, 0, json.Length);

            return FormatJson(rawJson);
        }

        private static string FormatJson(string json)
        {
            string INDENT_STRING = "  ";

            int indentation = 0;
            int quoteCount = 0;
            var result =
                from ch in json
                let quotes = ch == '"' ? quoteCount++ : quoteCount
                let lineBreak = ch == ',' && quotes % 2 == 0 ? ch + Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, indentation)) : null
                let openChar = ch == '{' || ch == '[' ? ch + Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, ++indentation)) : ch.ToString()
                let closeChar = ch == '}' || ch == ']' ? Environment.NewLine + String.Concat(Enumerable.Repeat(INDENT_STRING, --indentation)) + ch : ch.ToString()
                select lineBreak == null
                            ? openChar.Length > 1
                                ? openChar
                                : closeChar
                            : lineBreak;

            return String.Concat(result);
        }
    }

    public enum AdminMessageType
    {
        StatusUpdateRequest = 0,
        Taboo_Launch,
        Taboo_Shutdown,
        Taboo_Play,
        Taboo_Restart,
        Taboo_RestartClose,
        ShutdownPC,
        AttemptFix,
        Chat_Open,
        Chat_Close,
        Video_Opening_Open,
        Video_Opening_Close,
        Video_Trailer_Open,
        Video_Trailer_Close,
    }
}
