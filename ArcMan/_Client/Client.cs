﻿using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Timers;

using Microsoft.Toolkit.Uwp.Helpers;
using Microsoft.Toolkit.Uwp.UI.Controls;

using Windows.ApplicationModel;

namespace ArcMan
{
    [DataContract]
    public class Client : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public InAppNotification DebugNotification { get; set; }

        public void Cleanup(object s, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            StopErrorTimeout();
            _errorTimer?.Dispose();
            deferral.Complete();
        }

        public string DisplayName => $"PC{DeviceIndex}";

        [DataMember]
        public int DeviceIndex { get; set; }

        [DataMember]
        public ClientMessageType MessageType { get; set; }

        [DataMember]
        public bool IsGameHost { get; set; }

        [DataMember]
        public string HostName = Dns.GetHostName();

        [DataMember]
        public string MacAddress = Utils.GetDefaultMacAddress();

        [DataMember]
        public string IPAddress = Utils.GetLocalIPAddress().ToString();

        [DataMember]
        public int NowGameStep { get; set; }

        [DataMember]
        public ClientErrorType ErrorType { get; set; }

        [DataMember]
        public DateTimeOffset MessageTimeStamp { get; set; } = DateTimeOffset.MinValue;

        [DataMember]
        public string ConfigErrorFile { get; set; }

        [DataMember]
        public string ExceptionMessage { get; set; }
        // Used seperately by Admin and Client, not serialized
        public DisplayLanguage DisplayLanguage { get; set; }

        private ClientState _currentState;
        [DataMember]
        public ClientState CurrentState
        {
            get => _currentState;
            set
            {
                if (ShouldMonitorErrors)
                    ClientSideSetState(value);
                else
                    _currentState = value;
            }
        }

        private void ClientSideSetState(ClientState newState)
        {
            if (newState == _currentState)
                return;

            var previousState = _currentState;
            _currentState = newState;

            if (newState == ClientState.VR_Preparing)
                StartErrorTimeout(Constants.TIMEOUT_VR_START, new ClientErrorEventArgs(ClientState.VR_Preparing, ClientState.VR_Ready));
            else if (newState == ClientState.Game_Preparing)
                StartErrorTimeout(Constants.TIMEOUT_GAME_START, new ClientErrorEventArgs(ClientState.Game_Preparing, ClientState.Game_Ready));
            else if (newState != ClientState.Error)
                StopErrorTimeout();
            DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
            {
                DebugNotification.Show($"Transitioning from {previousState} to {newState}");
                await SendMessageToHostAsync(ClientMessageType.StateUpdate).ConfigureAwait(false);
            }).ConfigureAwait(false);
        }

        public void SyncProcessChanges()
        {
            var VR_Preparing = (!SteamVR_Running || !StarVR_Running);
            var VR_Ready = SteamVR_Running && StarVR_Running && StarVR_Operational;
            var Game_Preparing = VR_Ready && Taboo_Running && NowGameStep == -1;
            var Game_Ready = VR_Ready && Taboo_Running && NowGameStep == 0;
            var Game_Playing = VR_Ready && Taboo_Running && NowGameStep > 0;

            // skip restarting error timer for edge cases
            //var VR_Ready_ErrorLoop       = VR_Ready       && ErrorType == ClientErrorType.VRLaunchFailed;
            //var Game_Ready_ErrorLoop     = Game_Ready     && ErrorType == ClientErrorType.GameLaunchFailed;
            var VR_Preparing_ErrorLoop   = VR_Preparing && CurrentState == ClientState.Error && ErrorType == ClientErrorType.VRLaunchFailed;
            var StarVR_Frozen_ErrorLoop  = VR_Preparing && CurrentState == ClientState.Error && ErrorType == ClientErrorType.StarVR_Frozen;
            var Game_Preparing_ErrorLoop = Game_Preparing && CurrentState == ClientState.Error && ErrorType == ClientErrorType.GameLaunchFailed;
            var Game_Playing_ErrorLoop   = Game_Playing && CurrentState == ClientState.Error && ErrorType == ClientErrorType.TabooSceneOutOfSync;

            if (VR_Preparing_ErrorLoop || Game_Preparing_ErrorLoop || Game_Playing_ErrorLoop || StarVR_Frozen_ErrorLoop)
                return;

            if (!StarVR_Operational)
            {
                ErrorType = ClientErrorType.StarVR_Malfunctioned;
                CurrentState = ClientState.Error;
            }
            else if (Game_Playing)
                CurrentState = ClientState.Game_Playing;
            else if (Game_Ready)
                CurrentState = ClientState.Game_Ready;
            else if (Game_Preparing)
                CurrentState = ClientState.Game_Preparing;
            else if (VR_Ready)
                CurrentState = ClientState.VR_Ready;
            else if (VR_Preparing)
                CurrentState = ClientState.VR_Preparing;
        }

        public delegate void ErrorHandler(object sender, ClientErrorEventArgs e);
        private ElapsedEventHandler _errorTimeoutHandler = delegate { };
        public bool ShouldMonitorErrors { get; set; }
        private Timer _errorTimer;

        public Client()
        {
            App.Current.Suspending += Cleanup;
        }

        private void StartErrorTimeout(int seconds, ClientErrorEventArgs error)
        {
            DispatcherHelper.ExecuteOnUIThreadAsync(() =>
            {
                DebugNotification.Show($"Started error timeout for transition from {error.StartState}");
            });
            StopErrorTimeout();
            _errorTimer = new Timer { Interval = seconds * 1000, AutoReset = false, Enabled = false };
            _errorTimeoutHandler = new ElapsedEventHandler((s, e) => ErrorElapsed(error));
            _errorTimer.Elapsed += _errorTimeoutHandler;
            _errorTimer.Start();
        }

        private void StopErrorTimeout()
        {
            DispatcherHelper.ExecuteOnUIThreadAsync(() =>
            {
                DebugNotification?.Show($"Cancelled error timeout");
            });
            if (_errorTimer != null)
            {
                _errorTimer.Stop();
                _errorTimer.Elapsed -= _errorTimeoutHandler;
                ErrorType = ClientErrorType.None;
                _errorTimer.Close();
            }
        }

        private void ErrorElapsed(ClientErrorEventArgs e)
        {
            _errorTimer?.Close();
            if (_currentState != e.TargetState)
            {
                ErrorType = e.ErrorType;
                CurrentState = ClientState.Error;
                DispatcherHelper.ExecuteOnUIThreadAsync(() =>
                {
                    DebugNotification.Show($"Error occurred: {e.ErrorType}");
                });
            }
        }

        private bool _steamVR_Running;
        public bool SteamVR_Running
        {
            get => _steamVR_Running;
            set
            {
                if (_steamVR_Running != value)
                {
                    _steamVR_Running = value;
                    SyncProcessChanges();
                }
            }
        }

        private bool _starVR_Running;
        public bool StarVR_Running
        {
            get => _starVR_Running;
            set
            {
                if (_starVR_Running != value)
                {
                    // Reset StarVR_Operational on StarVR process exit so State transitions correctly to VR_Preparing and starts error timeout
                    if (_starVR_Running == true && value == false)
                        StarVR_Operational = true;

                    _starVR_Running = value;
                    SyncProcessChanges();
                }
            }
        }

        private bool _starVR_Operational = true;
        public bool StarVR_Operational
        {
            get => _starVR_Operational;
            set
            {
                if (_starVR_Operational != value)
                {
                    _starVR_Operational = value;
                    SyncProcessChanges();
                }
            }
        }
        private bool _taboo_Running;
        public bool Taboo_Running
        {
            get => _taboo_Running;
            set
            {
                if (_taboo_Running != value)
                {
                    // set GameHost to false in case Taboo is shutdown manually (not through Admin cmd)
                    if (_taboo_Running && !value)
                        IsGameHost = false;
                    _taboo_Running = value;
                    SyncProcessChanges();
                }
            }
        }

        public async Task<bool> SendMessageToHostAsync(ClientMessageType msgType)
        {
            this.MessageType = msgType;
            return await App.SessionManager.SendMessageToHostAsync(this).ConfigureAwait(false);
        }

        public void Clone(Client msg)
        {
            HostName     = msg.HostName;
            DeviceIndex  = msg.DeviceIndex;
            MessageType  = msg.MessageType;
            IPAddress    = msg.IPAddress;
            MacAddress   = msg.MacAddress;
            ErrorType    = msg.ErrorType;
            CurrentState = msg.CurrentState;
            NowGameStep  = msg.NowGameStep;
            IsGameHost   = msg.IsGameHost;
        }
    }
}
