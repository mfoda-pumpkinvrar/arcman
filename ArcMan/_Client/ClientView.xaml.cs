﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Microsoft.Toolkit.Uwp.Helpers;

using Windows.ApplicationModel;
using Windows.ApplicationModel.Core;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.System;
using Windows.System.Diagnostics;
using Windows.System.RemoteSystems;
using Windows.System.Threading;
using Windows.UI.Input.Preview.Injection;
using Windows.UI.Xaml.Controls;

namespace ArcMan
{
    public sealed partial class ClientView : Page, INotifyPropertyChanged
    {
        public bool IsConnected { get; set; }
        private InputInjector Keyboard = InputInjector.TryCreate();

        private InjectedInputKeyboardInfo Key_Enter_Down = new InjectedInputKeyboardInfo { VirtualKey = (ushort)VirtualKey.Enter };
        private InjectedInputKeyboardInfo Key_Enter_Up = new InjectedInputKeyboardInfo { VirtualKey = (ushort)VirtualKey.Enter, KeyOptions = InjectedInputKeyOptions.KeyUp };

        private InjectedInputKeyboardInfo Key_Back_Down = new InjectedInputKeyboardInfo { VirtualKey = (ushort)VirtualKey.Back };
        private InjectedInputKeyboardInfo Key_Back_Up = new InjectedInputKeyboardInfo { VirtualKey = (ushort)VirtualKey.Back, KeyOptions = InjectedInputKeyOptions.KeyUp };

        private InjectedInputKeyboardInfo Key_Escape_Down = new InjectedInputKeyboardInfo { VirtualKey = (ushort)VirtualKey.Escape };
        private InjectedInputKeyboardInfo Key_Escape_Up = new InjectedInputKeyboardInfo { VirtualKey = (ushort)VirtualKey.Escape, KeyOptions = InjectedInputKeyOptions.KeyUp };

        private IPropertySet settings = ApplicationData.Current.LocalSettings.Values;

        public event PropertyChangedEventHandler PropertyChanged;
        private int _startGameStep = 1;
        private int _playerNo = 1;

        public ClientView()
        {
            ApplicationData.Current.DataChanged += OnDataChanged;
            Initialize();


            ThreadPoolTimer.CreateTimer(async e =>
            {
                if (!IsConnected)
                    await RestartApp();
            }, TimeSpan.FromSeconds(Constants.TIMEOUT_CLIENT_CONNECTION));
        }

        private void OnDataChanged(ApplicationData sender, object args)
        {
            DispatcherHelper.ExecuteOnUIThreadAsync(() =>
                {
                    var client = App.Client;
                    client.SteamVR_Running = (settings["SteamVR_Running"] as bool?) ?? false;
                    client.StarVR_Running = (settings["StarVR_Running"] as bool?) ?? false;
                    client.Taboo_Running = (settings["Taboo_Running"] as bool?) ?? false;
                });
        }

        private async Task Cleanup(object s, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
            {
                ApplicationData.Current.DataChanged -= OnDataChanged;
                App.SessionManager.SessionFound -= SessionFound;
                App.SessionManager.SessionRemoved -= SessionRemoved; ;
                App.SessionManager.MessageReceived -= MessageReceived;
                App.SessionManager.DebugMessage -= DebugMessageReceived;
                await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("Cleanup");
            });
            deferral.Complete();
        }

        private async void Initialize()
        {
            InitializeComponent();
            App.Client.DebugNotification = DebugNotification;

            // ensure config files are not corrupted before access from either game or app
            await EnsureConfigFilesExist();

            StartNowGameStepMonitor();
            StartLogMonitor();

            await DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
            {
                App.Current.Suspending += async (s, e) => await Cleanup(s, e);
                await SyncPlayerNo();
                //await GetNowGameStepAndUpdate();
                //await ResetGameReady();
                await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("StartProcessMonitor");
                await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("OpenSteamVR");
                await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("OpenStarVR");
                App.SessionManager.SessionFound += SessionFound;
                App.SessionManager.SessionRemoved += SessionRemoved; ;
                App.SessionManager.SessionDisconnected += SessionManager_SessionDisconnected; ; ;
                App.SessionManager.MessageReceived += MessageReceived;
                App.SessionManager.DebugMessage += DebugMessageReceived;
                App.SessionManager.DiscoverSessions();
            });
        }

        private async Task EnsureConfigFilesExist()
        {
            // Game .ini files
            var configFolder = await StorageFolder.GetFolderFromPathAsync(Constants.TABOO_CONFIG_PATH);
            var configFiles = await configFolder.GetFilesAsync();
            // Default .ini files
            var defaultConfigFolder = await StorageFolder.GetFolderFromPathAsync(Constants.TABOO_DEFAULT_CONFIG_PATH);
            var defaultConfigFiles = await defaultConfigFolder.GetFilesAsync();

            foreach (var def in defaultConfigFiles)
            {
                var orig = configFiles.FirstOrDefault(f => f.Name == def.Name);

                var needsUpdate = false;
                // config file is missing
                if (orig == null)
                {
                    needsUpdate = true;

                    await def.CopyAsync(configFolder);
                    App.Client.ConfigErrorFile = def.Name;
                    await App.Client.SendMessageToHostAsync(ClientMessageType.ConfigMissing);
                }
                // config file is empty
                else
                {
                    var txt = await FileIO.ReadTextAsync(orig);
                    // Trim null-terminator
                    txt = txt.Trim('\0');
                    if (String.IsNullOrWhiteSpace(txt))
                    {
                        needsUpdate = true;
                        await def.CopyAndReplaceAsync(orig);

                        App.Client.ConfigErrorFile = def.Name;
                        await App.Client.SendMessageToHostAsync(ClientMessageType.ConfigCorrupted);
                    }
                }
                if (needsUpdate)
                {
                    await SyncPlayerNo();
                    await SetLanguage();
                    await SetGameHost();
                    await SetStartGameStep(_startGameStep);
                }
            }
        }

        private async void DebugMessageReceived(object sender, DebugMessageEventArgs e)
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(() =>
            {
                DebugNotification?.Show(e.Message);
            });
        }

        private async void SessionFound(object sender, SessionEventArgs e)
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
            {
                if (e.SessionInfo.DisplayName == Constants.SessionName)
                {
                    IsConnected = await App.SessionManager.JoinSession(e.SessionInfo, App.Client.DisplayName);
                    App.SessionManager.StartReceivingMessages();
                    // If Admin removed session then Client current state will still be Disconnected on session recreation
                    App.Client.SyncProcessChanges();
                    await App.Client.SendMessageToHostAsync(ClientMessageType.Join);
                }
            });
        }

        private async void SessionManager_SessionDisconnected(object sender, RemoteSystemSessionDisconnectedEventArgs e) =>
            await DispatcherHelper.ExecuteOnUIThreadAsync(() => IsConnected = false);

        private async void SessionRemoved(object sender, SessionEventArgs e) =>
            await DispatcherHelper.ExecuteOnUIThreadAsync(() => IsConnected = false);

        private async void MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
            {
                IsConnected = true;

                object data = new AdminMessage();
                e.GetDeserializedMessage(ref data);
                var msg = data as AdminMessage;

                DebugNotification?.Show($"Received messsage: {msg.MessageType} from Admin");

                //switch (msg.MessageType)
                //{
                //    case AdminMessageType.Taboo_Launch:
                //    case AdminMessageType.Taboo_Shutdown:
                //    case AdminMessageType.Taboo_Play:
                //    case AdminMessageType.Taboo_Restart:
                //    case AdminMessageType.ShutdownPC:
                //    case AdminMessageType.AttemptFix:
                //        //StopGameSyncMonitoring();
                //        break;
                //}

                switch (msg.MessageType)
                {
                    case AdminMessageType.StatusUpdateRequest:
                        App.Client.SyncProcessChanges();
                        await App.Client.SendMessageToHostAsync(ClientMessageType.StateUpdate);
                        DebugNotification?.Show($"Sending Status update");
                        break;
                    case AdminMessageType.Taboo_Launch:
                        if (App.Client.CurrentState != ClientState.VR_Ready)
                            return;
                        DebugNotification?.Show($"Launching Taboo");
                        App.Client.IsGameHost = msg.GameHostIndex == App.Client.DeviceIndex;
                        await SetLanguage(msg);
                        await SetGameHost();
                        await PrepareLaunchTaboo();
                        if (!LanguageIsNone(msg))
                            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("OpenTaboo");
                        break;
                    case AdminMessageType.Taboo_Play:
                        if (App.Client.CurrentState != ClientState.Game_Ready)
                            return;
                        DebugNotification?.Show($"Playing Taboo");
                        await SetLanguage(msg);
                        if (App.Client.IsGameHost)
                        {
                            _startGameStep = msg.GameStep;
                            await SetStartGameStep(msg.GameStep);
                            await Task.Delay(2000);
                            Keyboard.InjectKeyboardInput(new[] { Key_Enter_Down, Key_Enter_Up });
                        }
                        break;
                    case AdminMessageType.Taboo_Shutdown:
                        var validState = App.Client.CurrentState == ClientState.Game_Ready || App.Client.CurrentState == ClientState.Game_Playing;
                        if (validState && msg.CloseTabooDeviceIndex == App.Client.DeviceIndex)
                        {
                            App.Client.IsGameHost = false;
                            Keyboard.InjectKeyboardInput(new[] { Key_Escape_Down, Key_Escape_Up });
                            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("CloseTaboo");
                            await RestartProcesses(steamvr: true, starvr: true, taboo: false);
                        }
                        break;
                    case AdminMessageType.Taboo_Restart:
                        if (App.Client.CurrentState != ClientState.Game_Playing && App.Client.CurrentState != ClientState.Game_Ready)
                            return;
                        DebugNotification?.Show($"Restarting Taboo");
                        await PrepareLaunchTaboo();
                        Keyboard.InjectKeyboardInput(new[] { Key_Back_Down, Key_Back_Up });
                        break;
                    case AdminMessageType.Taboo_RestartClose:
                        if (App.Client.CurrentState != ClientState.Game_Playing && App.Client.CurrentState != ClientState.Game_Ready)
                            return;
                        DebugNotification?.Show($"Close Restarting Taboo");
                        App.Client.IsGameHost = msg.GameHostIndex == App.Client.DeviceIndex;
                        await SetGameHost();
                        await PrepareLaunchTaboo();
                        Keyboard.InjectKeyboardInput(new[] { Key_Back_Down, Key_Back_Up });
                        break;
                    case AdminMessageType.ShutdownPC:
                        if (msg.ShutDownDeviceIndex == App.Client.DeviceIndex)
                            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("Shutdown");
                        break;
                    case AdminMessageType.AttemptFix:
                        if (App.Client.CurrentState != ClientState.Error)
                            return;
                        DebugNotification?.Show($"Attempting fix");
                        await AttemptFix();
                        break;
                    case AdminMessageType.Chat_Open:
                        settings["MumbleHostIP"] = msg.ChatHostIP;
                        await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("ChatClose");
                        await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("ChatOpenClient");

                        //if (msg.VideoPlayIndex == App.Client.DeviceIndex)
                        //    await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("ChatOpenHost");
                        //else
                        break;
                    case AdminMessageType.Chat_Close:
                        await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("ChatClose");
                        break;
                    case AdminMessageType.Video_Opening_Open:
                        if (msg.VideoPlayIndex == App.Client.DeviceIndex)
                            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("OpeningVideoOpen");
                        break;
                    case AdminMessageType.Video_Trailer_Open:
                        if (msg.VideoPlayIndex == App.Client.DeviceIndex)
                            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("TrailerVideoOpen");
                        break;
                    case AdminMessageType.Video_Opening_Close:
                    case AdminMessageType.Video_Trailer_Close:
                        await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("CloseVLC");
                        break;
                }
            });
        }

        private bool LanguageIsNone(AdminMessage msg)
        {
            var isNone = false;
            switch (App.Client.DeviceIndex)
            {
                case 1:
                    isNone = msg.PC1Language == DisplayLanguage.NONE;
                    break;
                case 2:
                    isNone = msg.PC2Language == DisplayLanguage.NONE;
                    break;
                case 3:
                    isNone = msg.PC3Language == DisplayLanguage.NONE;
                    break;
                case 4:
                    isNone = msg.PC4Language == DisplayLanguage.NONE;
                    break;
            }
            return isNone;
        }

        private List<int> _gameStepTimeOffsets = new List<int> { 0, 140, 170, 50, 80, 55, 65 };
        private List<ThreadPoolTimer> _gameStepTimers = new List<ThreadPoolTimer>();
        private ThreadPoolTimer _gameStepTimer;
        private void StartGameSyncMonitoring(int nowGameStep)
        {
            StopGameSyncMonitoring();

            // case when taboo crashes, monitoring should not start
            if (!App.Client.Taboo_Running)
                return;
            CreateGameSyncTimer(nowGameStep, 15);
        }

        private void CreateGameSyncTimer(int targetStep, int timeout)
        {
            StopGameSyncMonitoring();
            _gameStepTimer = ThreadPoolTimer.CreateTimer(
                 async _ =>
                 {
                     var file = await StorageFile.GetFileFromPathAsync(Constants.TABOO_STEP_CONFIG);
                     var txt = await FileIO.ReadTextAsync(file);
                     var step = int.Parse(Regex.Match(txt, @"NowGameStep=(.+)").Groups[1].Value);
                     if (step != targetStep)
                         await DispatcherHelper.ExecuteOnUIThreadAsync(() =>
                         {
                             App.Client.ErrorType = ClientErrorType.TabooSceneOutOfSync;
                             App.Client.CurrentState = ClientState.Error;
                         });
                     else
                     {
                         if (targetStep == _gameStepTimeOffsets.Count)
                             return;
                         CreateGameSyncTimer(targetStep + 1, _gameStepTimeOffsets[targetStep]);
                     }
                 }, TimeSpan.FromSeconds(timeout));
        }

        private void StopGameSyncMonitoring() => _gameStepTimer?.Cancel();

        private async Task AttemptFix()
        {
            switch (App.Client.ErrorType)
            {
                case ClientErrorType.SessionConnectionFailed:
                    await RestartApp();
                    break;
                case ClientErrorType.VRLaunchFailed:
                case ClientErrorType.StarVR_Malfunctioned:
                case ClientErrorType.StarVR_Frozen:
                case ClientErrorType.GameLaunchFailed:
                case ClientErrorType.TabooSceneOutOfSync:
                    // close taboo to avoid bug when taboo is running and vr process crashes
                    await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("CloseTaboo");
                    await RestartProcesses(steamvr: true, starvr: true, taboo: false, delay: 0);
                    break;
            }
        }

        private async Task RestartProcesses(bool steamvr, bool starvr, bool taboo, int delay = 5000)
        {
            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("CloseTaboo");
            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("CloseStarVR");
            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("CloseSteamVR");

            var starvrExited = await ConfirmStarVRExit();

            // Transition to error state and allow operator to try fix
            if (!starvrExited)
            {
                App.Client.ErrorType = ClientErrorType.StarVR_Frozen;
                App.Client.CurrentState = ClientState.Error;
                return; 
            }

            // Reset so CurrentState can transition to VR_Preparing and wait for new log entry
            App.Client.StarVR_Operational = true; 
            if (steamvr)
                await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("OpenSteamVR");
            if (starvr)
                await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("OpenStarVR");
            if (taboo)
            {
                await PrepareLaunchTaboo();
                await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("OpenTaboo");
            }
        }

        private async Task<bool> ConfirmStarVRExit(int maxRetry = 4, int checkInterval = 3000)
        {
            var processes = ProcessDiagnosticInfo.GetForProcesses();

            var stillAlive = processes.Any(p => p.ExecutableFileName.ToLower() == "starvrsystem.exe");

            if (!stillAlive)
                return true;
            if (maxRetry == 0)
                return false;

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("CloseStarVR");

            // wait here so previous tests can exit early and we don't do an extra wait
            await Task.Delay(checkInterval);

            return await ConfirmStarVRExit(--maxRetry, checkInterval);
        }

        private async Task PrepareLaunchTaboo()
        {
            //StopGameSyncMonitoring();
            await ResetGameReady();
        }

        private void StartLogMonitor()
        {
            Task.Run(async () =>
            {
                try
                {
                    var interval = 1000;
                    var path = Path.Combine(UserDataPaths.GetDefault().LocalAppData, "StarVR_Log", "StarVRSystem.txt");
                    var file = await StorageFile.GetFileFromPathAsync(path);
                    using (var fs = await file.OpenAsync(FileAccessMode.Read, StorageOpenOptions.AllowReadersAndWriters))
                    using (var stream = new StreamReader(fs.AsStream()))
                    {
                        // Skip initial entries
                        stream.ReadToEnd();
                        while (true)
                        {
                            var s = stream.ReadLine();
                            if (!string.IsNullOrEmpty(s))
                            {
                                await DispatcherHelper.ExecuteOnUIThreadAsync(() => DebugNotification?.Show(s));

                                if (s.Contains(Constants.STARVR_LOG_MSG_ERROR_1) || s.Contains(Constants.STARVR_LOG_MSG_ERROR_2))
                                    App.Client.StarVR_Operational = false;
                                else if (s.Contains(Constants.STARVR_LOG_MSG_SUCCESS))
                                    App.Client.StarVR_Operational = true;
                            }
                            await Task.Delay(interval);
                        }
                    }
                }
                catch (Exception)
                {
                    // handle case where StarVR was launched before App and Log file is locked
                    await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("CloseStarVR");
                    await Task.Delay(5000);
                    await RestartApp();
                }
            });
        }

        private void StartNowGameStepMonitor()
        {
            Task.Run(async () =>
            {
                var interval = 1000;
                var file = await StorageFile.GetFileFromPathAsync(Constants.TABOO_STEP_CONFIG);
                while (true)
                {
                    try
                    {
                        using (var stream = await file.OpenAsync(FileAccessMode.Read, StorageOpenOptions.AllowReadersAndWriters))
                        using (var input = stream.GetInputStreamAt(0))
                        using (var reader = new Windows.Storage.Streams.DataReader(input))
                        {
                            uint bytes = await reader.LoadAsync((uint)stream.Size);
                            var txt = reader.ReadString(bytes);
                            var match = Regex.Match(txt, @"^NowGameStep=(-?\d+)$");
                            if (match.Success)
                            {
                                var oldStep = App.Client.NowGameStep;
                                var newStep = int.Parse(match.Groups[1].Value);
                                App.Client.NowGameStep = newStep;
                                if (oldStep != newStep)
                                {
                                    App.Client.SyncProcessChanges();
                                    await DispatcherHelper.ExecuteOnUIThreadAsync(async () => await App.Client.SendMessageToHostAsync(ClientMessageType.StateUpdate));
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        await Task.Delay(interval);
                        continue;
                    }
                    await Task.Delay(interval);
                }
            });
        }

        private async Task RestartApp()
        {
            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("BringToFront");
            await DispatcherHelper.ExecuteOnUIThreadAsync(() => WaitingText.Text = "再起動...");
            await Task.Delay(5000);
            await CoreApplication.RequestRestartAsync($"PC{App.Client.DeviceIndex}");
        }
        private async Task WriteConfig(string path, string pattern, string replacement, int retry = 5)
        {
            try
            {
                var file = await StorageFile.GetFileFromPathAsync(path);
                using (var stream = await file.OpenAsync(FileAccessMode.ReadWrite, StorageOpenOptions.AllowReadersAndWriters))
                using (var input = stream.GetInputStreamAt(0))
                using (var output = stream.GetOutputStreamAt(0))
                using (var reader = new Windows.Storage.Streams.DataReader(input))
                using (var writer = new Windows.Storage.Streams.DataWriter(output))
                {
                    uint bytes = await reader.LoadAsync((uint)stream.Size);
                    var oldContent = reader.ReadString(bytes);

                    if (String.IsNullOrEmpty(oldContent.Trim('\0')))
                    {
                        await EnsureConfigFilesExist();
                        return;
                    }

                    var newContent = Regex.Replace(oldContent, pattern, replacement, RegexOptions.Multiline);
                    writer.WriteString(newContent);

                    stream.Size = await writer.StoreAsync();
                    await writer.FlushAsync();
                }
            }
            catch (Exception)
            {
                await Task.Delay(500);
                if (retry > 0)
                    await WriteConfig(path, pattern, replacement, --retry);
                else
                {
                    App.Client.ConfigErrorFile = $"{path} ({replacement})";
                    await App.Client.SendMessageToHostAsync(ClientMessageType.IOFailure);
                }
            }
        }

        private async Task<string> ReadConfig(string path, string pattern, string defaultValue)
        {
            try
            {
                var file = await StorageFile.GetFileFromPathAsync(path);
                using (var stream = await file.OpenAsync(FileAccessMode.Read, StorageOpenOptions.AllowReadersAndWriters))
                {
                    var content = await stream.ReadTextAsync();
                    var match = Regex.Match(content, pattern, RegexOptions.IgnoreCase);
                    return match.Groups[1].Value;
                }
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        private async Task SyncPlayerNo()
        {
            var seatingIndex = 0;
            switch (App.Client.DeviceIndex)
            {
                case 1:
                    seatingIndex = 4;
                    break;
                case 2:
                    seatingIndex = 2;
                    break;
                case 3:
                    seatingIndex = 1;
                    break;
                case 4:
                    seatingIndex = 3;
                    break;
            }
            await WriteConfig(Constants.TABOO_GAME_CONFIG, @"^PlayerNo=.*$", $"PlayerNo={seatingIndex}");
        }

        private async Task SetLanguage()
        {
            await WriteConfig(Constants.TABOO_LANG_CONFIG, @"^Language=.*$", $"Language={Constants.LangCodeLookup[App.Client.DisplayLanguage]}");
        }

        private async Task SetLanguage(AdminMessage msg)
        {
            var lang = DisplayLanguage.JA_JP;
            switch (App.Client.DeviceIndex)
            {
                case 1:
                    lang = msg.PC1Language;
                    break;
                case 2:
                    lang = msg.PC2Language;
                    break;
                case 3:
                    lang = msg.PC3Language;
                    break;
                case 4:
                    lang = msg.PC4Language;
                    break;
            }
            App.Client.DisplayLanguage = lang;
            await WriteConfig(Constants.TABOO_LANG_CONFIG, @"^Language=.*$", $"Language={Constants.LangCodeLookup[lang]}");
        }

        private async Task SetStartGameStep(int gameStep)
        {
            _startGameStep = gameStep;
            await WriteConfig(Constants.TABOO_GAME_CONFIG, @"^StartGameStep=.*$", $"StartGameStep={gameStep}");
        }
        private async Task SetGameHost()
        {
            await WriteConfig(Constants.TABOO_GAME_CONFIG, @"^IsServer=.*$", $"IsServer={(App.Client.IsGameHost ? "True" : "False")}");
        }
        private async Task ResetGameReady()
        {
            await WriteConfig(Constants.TABOO_STEP_CONFIG, @"^NowGameStep=.*$", "NowGameStep=-1");
        }
    }
}
