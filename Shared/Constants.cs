﻿using System.Collections.Generic;

namespace ArcMan
{
    public static class Constants
    {
        public static string SessionName = "ArcMan";
        //{
        //    Assembly assembly = Assembly.GetExecutingAssembly();
        //    FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
        //    string version = fileVersionInfo.ProductVersion;
        //    return $"ARCMAN_{version}";
        //}

        public static readonly Dictionary<DisplayLanguage, string> LangCodeLookup = new Dictionary<DisplayLanguage, string>()
        {
            { DisplayLanguage.ZH_CN, "SEGA_CN" },
            { DisplayLanguage.JA_JP, "SEGA_JP" },
            { DisplayLanguage.KO_KR, "Korea" },
            { DisplayLanguage.EN_US, "SEGA_EN" },
            { DisplayLanguage.NONE,  "NONE" }
        };

        public static readonly string TABOO_FOLDER_PATH = @"C:\PumpkinStudio\Taboo\CreepyVoodooDoll";
        public static readonly string TABOO_CONFIG_PATH = TABOO_FOLDER_PATH + @"\Content\LanguageSetting";
        public static readonly string TABOO_GAME_CONFIG = TABOO_CONFIG_PATH + @"\HeadsetSetting.ini";
        public static readonly string TABOO_DEFAULT_CONFIG_PATH = TABOO_FOLDER_PATH + @"\Content\DefaultSetting";
        public static readonly string TABOO_STEP_CONFIG = TABOO_CONFIG_PATH + @"\NowGameStep.ini";
        public static readonly string TABOO_LANG_CONFIG = TABOO_CONFIG_PATH + @"\LanguageSetting.ini";
        public static readonly string TABOO_OPENING_VIDEO_PATH = @"C:\PumpkinStudio\Taboo\TabooOpening.mp4";
        public static readonly string TABOO_TRAILER_VIDEO_PATH = @"C:\PumpkinStudio\Taboo\TabooTrailer.mp4";
        public static readonly string APP_ERROR_CONFIG = @"C:\PumpkinStudio\Taboo\ErrorSetting.ini";
        public static readonly string APP_EVENT_LOG_PATH = @"C:\PumpkinStudio\Taboo\EventLog";
        public const string STARVR_LOG_MSG_ERROR_1 = @"[ ERROR ] StarVR: Make sure all the cables are properly connected";
        public const string STARVR_LOG_MSG_ERROR_2 = @"[ ERROR ] StarVR: HMD Tracking type is not valid";
        public const string STARVR_LOG_MSG_SUCCESS = @"[ INFO  ] StarVR System: StarVR READY";
        public static int TIMEOUT_CLIENT_CONNECTION { get; set; } = 60;
        public static int TIMEOUT_ADMIN_CONNECTION { get; set; } = 60;
        public static int TIMEOUT_VR_START { get; set; } = 60;
        public static int TIMEOUT_GAME_START { get; set; } = 60;
        public const string TABOO_PROCESS_NAME = "CreepyVoodooDoll-Win64-Shipping";
        public const string STARVR_PROCESS_NAME = "StarVRSystem";
        public const string STEAMVR_PROCESS_NAME = "vrmonitor";
        public static readonly string TABOO_EXE_PATH    = @"C:\PumpkinStudio\Taboo\CreepyVoodooDoll\Binaries\Win64\CreepyVoodooDoll-Win64-Shipping.exe";
        public static readonly string STEAMVR_EXE_PATH  = @"C:\Program Files (x86)\Steam\steamapps\common\SteamVR\bin\win64\vrmonitor.exe";
        public static readonly string STARVR_EXE_PATH = @"C:\PumpkinStudio\StarVR\bin\StarVRSystem.exe";
    }
}
