﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.ExtendedExecution.Foreground;
using Windows.Foundation;

namespace ArcMan
{
    public static class ExtendedExecutionForegroundHelper
    {
        private static ExtendedExecutionForegroundSession session = null;
        private static int taskCount = 0;

        public static bool IsRunning
        {
            get
            {
                if (session != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static async Task<ExtendedExecutionForegroundResult> RequestSessionAsync(ExtendedExecutionForegroundReason reason, TypedEventHandler<object, ExtendedExecutionForegroundRevokedEventArgs> revoked, String description)
        {
            // The previous Extended Execution must be closed before a new one can be requested.       
            ClearSession();

            var newSession = new ExtendedExecutionForegroundSession();
            newSession.Reason = reason;
            newSession.Description = description;
            newSession.Revoked += SessionRevoked;

            // Add a revoked handler provided by the app in order to clean up an operation that had to be halted prematurely
            if (revoked != null)
            {
                newSession.Revoked += revoked;
            }

            ExtendedExecutionForegroundResult result = await newSession.RequestExtensionAsync();

            switch (result)
            {
                case ExtendedExecutionForegroundResult.Allowed:
                    session = newSession;
                    break;
                default:
                case ExtendedExecutionForegroundResult.Denied:
                    newSession.Dispose();
                    break;
            }
            return result;
        }

        public static void ClearSession()
        {
            if (session != null)
            {
                session.Dispose();
                session = null;
            }

            taskCount = 0;
        }

        public static Deferral GetExecutionDeferral()
        {
            if (session == null)
            {
                throw new InvalidOperationException("No extended execution session is active");
            }

            taskCount++;
            return new Deferral(OnTaskCompleted);
        }

        private static void OnTaskCompleted()
        {
            if (taskCount > 0)
            {
                taskCount--;
            }

            //If there are no more running tasks than end the extended lifetime by clearing the session
            if (taskCount == 0 && session != null)
            {
                ClearSession();
            }
        }

        private static void SessionRevoked(object sender, ExtendedExecutionForegroundRevokedEventArgs args)
        {
            //The session has been prematurely revoked due to system constraints, ensure the session is disposed
            if (session != null)
            {
                session.Dispose();
                session = null;
            }

            taskCount = 0;
        }
    }
}