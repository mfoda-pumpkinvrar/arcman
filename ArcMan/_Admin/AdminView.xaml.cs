﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Toolkit.Uwp.Helpers;

using Windows.ApplicationModel;
using Windows.Storage;
using Windows.System.Threading;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace ArcMan
{
    public sealed partial class AdminView : Page, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private const int AllowedResponseDelay = 10;
        public Client PC1 = new Client { DeviceIndex = 1 };
        public Client PC2 = new Client { DeviceIndex = 2 };
        public Client PC3 = new Client { DeviceIndex = 3 };
        public Client PC4 = new Client { DeviceIndex = 4 };
        private List<Client> _clients;

        private int _videoDeviceIndex { get; set; }

        public bool PC1_LangNone => PC1_State == ClientState.VR_Ready;
        public bool PC2_LangNone => PC2_State == ClientState.VR_Ready;
        public bool PC3_LangNone => PC3_State == ClientState.VR_Ready;
        public bool PC4_LangNone => PC4_State == ClientState.VR_Ready;

        public bool PC1_Set_Video_Enabled => PC1_State != ClientState.Disconnected && PC1_State != ClientState.Error;
        public bool PC2_Set_Video_Enabled => PC2_State != ClientState.Disconnected && PC2_State != ClientState.Error;
        public bool PC3_Set_Video_Enabled => PC3_State != ClientState.Disconnected && PC3_State != ClientState.Error;
        public bool PC4_Set_Video_Enabled => PC4_State != ClientState.Disconnected && PC4_State != ClientState.Error;
        public string PC1_Video_Indicator => $"/Assets/P_Button_Main_04_{(_videoDeviceIndex == 1 ? "Blue" : "Black")}_01.png";
        public string PC2_Video_Indicator => $"/Assets/P_Button_Main_04_{(_videoDeviceIndex == 2 ? "Blue" : "Black")}_02.png";
        public string PC3_Video_Indicator => $"/Assets/P_Button_Main_04_{(_videoDeviceIndex == 3 ? "Blue" : "Black")}_03.png";
        public string PC4_Video_Indicator => $"/Assets/P_Button_Main_04_{(_videoDeviceIndex == 4 ? "Blue" : "Black")}_04.png";
        // State is extracted into seperate field since INotifyProperty doesn't fire on nested objects
        public ClientState PC1_State { get; set; }
        public ClientState PC2_State { get; set; }
        public ClientState PC3_State { get; set; }
        public ClientState PC4_State { get; set; }
        private List<ClientState> _clientStates;

        public bool CanEditLanguage { get; set; } = true;
        public int EarliestGameStepReported { get; set; }
        public int DisplayedGameSceneIndex { get; set; }
        public bool IsBusy { get; set; }
        private ThreadPoolTimer _PC1_timer { get; set; }
        private ThreadPoolTimer _PC2_timer { get; set; }
        private ThreadPoolTimer _PC3_timer { get; set; }
        private ThreadPoolTimer _PC4_timer { get; set; }

        public string PC1_Indicator => Indicator(1, PC1_State);
        public string PC2_Indicator => Indicator(2, PC2_State);
        public string PC3_Indicator => Indicator(3, PC3_State);
        public string PC4_Indicator => Indicator(4, PC4_State);
        private string Indicator(int i, ClientState state) => $"/Assets/P_Button_Main_04_{state.GetColor()}_0{i}.png";

        public string PC1_Open_Icon => OpenIcon(1, PC1_State);
        public string PC2_Open_Icon => OpenIcon(2, PC2_State);
        public string PC3_Open_Icon => OpenIcon(3, PC3_State);
        public string PC4_Open_Icon => OpenIcon(4, PC4_State);
        private string OpenIcon(int i, ClientState state) => $"/Assets/Light_{(state == ClientState.Game_Ready || state == ClientState.Game_Playing ? "Blue" : "Black")}_0{i}.png";

        public string PC1_Start_Icon => StartIcon(1, PC1_State);
        public string PC2_Start_Icon => StartIcon(2, PC2_State);
        public string PC3_Start_Icon => StartIcon(3, PC3_State);
        public string PC4_Start_Icon => StartIcon(4, PC4_State);
        private string StartIcon(int i, ClientState state) => $"/Assets/Light_{(state == ClientState.Game_Playing ? "Blue" : "Black")}_0{i}.png";

        public string PC1_Lang_UI => Lang_UI(1, PC1_State);
        public string PC2_Lang_UI => Lang_UI(2, PC2_State);
        public string PC3_Lang_UI => Lang_UI(3, PC3_State);
        public string PC4_Lang_UI => Lang_UI(4, PC4_State);
        private string Lang_UI(int i, ClientState state) =>
            $"/Assets/AI_Button_Main_01_{(state == ClientState.Disconnected || _clients[i - 1].ErrorType == ClientErrorType.SessionConnectionFailed ? "Black" : "Blue")}_0{i}.png";

        public static bool Any(StatusColor color, IEnumerable<ClientState> list) => list.Any(state => ClientStateExt.GetColor(state) == color);
        public static bool All(StatusColor color, IEnumerable<ClientState> list) => list.All(state => ClientStateExt.GetColor(state) == color);
        public static bool None(StatusColor color, IEnumerable<ClientState> list) => list.All(state => ClientStateExt.GetColor(state) != color);

        public static bool AllEither(StatusColor c1, StatusColor c2, StatusColor c3, StatusColor c4, IEnumerable<ClientState> list) =>
            list.All(state => ClientStateExt.GetColor(state) == c1 || ClientStateExt.GetColor(state) == c2 || ClientStateExt.GetColor(state) == c3 || ClientStateExt.GetColor(state) == c4);
        public static bool AllEither(StatusColor c1, StatusColor c2, StatusColor c3, IEnumerable<ClientState> list) =>
            list.All(state => ClientStateExt.GetColor(state) == c1 || ClientStateExt.GetColor(state) == c2 || ClientStateExt.GetColor(state) == c3);
        public static bool AllEither(StatusColor c1, StatusColor c2, IEnumerable<ClientState> list) =>
            list.All(state => ClientStateExt.GetColor(state) == c1 || ClientStateExt.GetColor(state) == c2);
        public string OpenButtonImage => $"/Assets/AI_Button_Open_{(OpenGlowing ? "Blue" : (IsOpenEnabled ? "Black" : "Dark"))}.png";
        public string StartButtonImage => $"/Assets/AI_Button_Start_{(StartGlowing ? "Blue" : (IsStartEnabled ? "Black" : "Dark"))}.png";

        public string CloseButtonImage => $"/Assets/AI_Button_Close_{(IsCloseEnabled ? "Black" : "Dark")}.png";
        public string RestartButtonImage => $"/Assets/AI_Button_Restart_{(IsRestartEnabled ? "Black" : "Dark")}.png";
        public string FixButtonImage => $"/Assets/AI_Button_Fix_{(IsFixEnabled ? "Black" : "Dark")}.png";

        public string NextButtonImage
        {
            get
            {
                var list = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };
                return $"/Assets/P_Button_Next_{((All(StatusColor.Black, list) || _clients.All(c => c.ErrorType == ClientErrorType.SessionConnectionFailed)) ? "Blue" : "Black")}.png";
            }
        }
        public bool OpenGlowing
        {
            get
            {
                var list = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };
                return Any(StatusColor.Green, list) || Any(StatusColor.Green_Play, list);
            }
        }
        public bool IsOpenEnabled
        {
            get
            {
                var list = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };
                return Any(StatusColor.Blue, list) && AllEither(StatusColor.Blue, StatusColor.Green, StatusColor.Red, StatusColor.Black, list);
            }
        }
        public bool IsCloseEnabled
        {
            get
            {
                var list = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };
                return (Any(StatusColor.Green, list) || Any(StatusColor.Green_Play, list))
                        && None(StatusColor.Yellow, list);
            }
        }

        public bool StartGlowing
        {
            get
            {
                var list = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };
                return Any(StatusColor.Green_Play, list);
            }
        }
        public bool IsStartEnabled
        {
            get
            {
                var list = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };
                return Any(StatusColor.Green, list) && AllEither(StatusColor.Blue, StatusColor.Green, StatusColor.Red, StatusColor.Black, list);
            }
        }
        public bool IsRestartEnabled
        {
            get
            {
                var list = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };
                return Any(StatusColor.Green_Play, list) && None(StatusColor.Yellow, list);
            }
        }
        public bool IsFixEnabled
        {
            get
            {
                var list = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };
                return Any(StatusColor.Red, list);
            }
        }

        private Client FindGameHostCandidate()
        {
            // prioritize clients by seating arrangement
            var orderedClients = new List<Client> { _clients[2], _clients[1], _clients[3], _clients[0] };
            // return existing host in case game is already running
            if (orderedClients.Any(c => c.IsGameHost))
                return orderedClients.Find(c => c.IsGameHost);

            var gameReady = orderedClients.FirstOrDefault(c => _clientStates[c.DeviceIndex - 1] == ClientState.Game_Ready && c.DisplayLanguage != DisplayLanguage.NONE);
            var vrReady = orderedClients.FirstOrDefault(c => _clientStates[c.DeviceIndex - 1] == ClientState.VR_Ready && c.DisplayLanguage != DisplayLanguage.NONE);
            return gameReady ?? vrReady;
        }

        public bool PC1_GameStep_Enabled => PC1_State == ClientState.Game_Playing;
        public bool PC2_GameStep_Enabled => PC2_State == ClientState.Game_Playing;
        public bool PC3_GameStep_Enabled => PC3_State == ClientState.Game_Playing;
        public bool PC4_GameStep_Enabled => PC4_State == ClientState.Game_Playing;

        public AdminView()
        {
            App.Current.Suspending += async (s, e) => await Cleanup(s, e);


            _clients = new List<Client> { PC1, PC2, PC3, PC4 };
            _clientStates = new List<ClientState> { PC1_State, PC2_State, PC3_State, PC4_State };

            InitSessionManager();
            ApplicationView.GetForCurrentView().TryEnterFullScreenMode();

            //RemoveDebugFunctionality();
        }
        private void RemoveDebugFunctionality()
        {
            DebugButton.Visibility = Visibility.Collapsed;
        }

        private async Task Cleanup(object s, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await LogEvent("UI", new List<string> { "=============================== App Closed ===============================" });
            App.SessionManager.MessageReceived -= MessageReceived;
            App.SessionManager.DebugMessage -= DebugMessageReceived;
            App.SessionManager.EndSession();
            deferral.Complete();
        }

        private async void InitSessionManager()
        {
            InitializeComponent();

            await CleanupEventLog();
            await DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
            {
                App.SessionManager.MessageReceived += MessageReceived;
                App.SessionManager.ParticipantJoined += SessionManager_ParticipantJoined;
                App.SessionManager.ParticipantLeft += SessionManager_ParticipantLeft;
                App.SessionManager.SessionDisconnected += SessionManager_SessionDisconnected;
                App.SessionManager.DebugMessage += DebugMessageReceived;
                var status = await App.SessionManager.CreateSession(Constants.SessionName);
                App.SessionManager.StartReceivingMessages();
                ThreadPoolTimer.CreatePeriodicTimer(RequestStateUpdate, TimeSpan.FromSeconds(3));
            });
        }

        // Delete any log files older than 7 days
        private async Task CleanupEventLog()
        {
            var rootFolder = await StorageFolder.GetFolderFromPathAsync(@"C:\PumpkinStudio\Taboo");
            var logFolder = await rootFolder.CreateFolderAsync("EventLog", CreationCollisionOption.OpenIfExists);

            // Clearing log file not required
            //var logFiles = await logFolder.GetFilesAsync();
            //if (logFiles.Count > 7)
            //{
            //    var keep = logFiles.OrderByDescending(f => f.DateCreated).Take(7);
            //    var remove = logFiles.Except(keep);
            //    foreach (var f in remove)
            //        await f.DeleteAsync();
            //}
        }

        private async Task LogEvent(string eventSource, List<string> msgs)
        {
            var folder = await StorageFolder.GetFolderFromPathAsync(Constants.APP_EVENT_LOG_PATH);
            var name = $"{DateTime.Now.DayOfWeek} {DateTime.Now.Month}-{DateTime.Now.Day}.txt";
            var log = await folder.CreateFileAsync(name, CreationCollisionOption.OpenIfExists);
            var prefix = $"[{DateTime.Now.ToLongTimeString()}] [{eventSource}] : ";
            using (var ras = await log.OpenAsync(FileAccessMode.ReadWrite, StorageOpenOptions.AllowReadersAndWriters))
            using (var outs = ras.GetOutputStreamAt(ras.Size))
            using (var writer = new Windows.Storage.Streams.DataWriter(outs))
            {
                // Append first line
                writer.WriteString($"{prefix}{msgs[0]}\n");

                // Append other lines padded and without time signature
                if (msgs.Count > 1)
                    for (int i = 1; i < msgs.Count; i++)
                        writer.WriteString($"{"".PadLeft(prefix.Length)}{msgs[i]}\n");

                await writer.StoreAsync();
                await outs.FlushAsync();
            }

        }

        private async Task LogAdminMessage(AdminMessage msg)
        {
            var msgs = new List<string>() { "Sent Message =>" };
            msgs.AddRange(msg.ToString().Split('\n', StringSplitOptions.RemoveEmptyEntries));

            await LogEvent("Admin", msgs);
        }

        private async Task LogClientStateChange(int deviceIndex, ClientState newState)
        {
            var client = _clients[deviceIndex - 1];
            var currState = _clientStates[deviceIndex - 1];
            if (currState == newState)
                return;

            var _newState = $"{newState} {(newState == ClientState.Error ? client.ErrorType.ToString() : "")}";
            var msg = $"{currState} => {_newState}";
            await LogEvent($"PC{client.DeviceIndex}", new List<string> { msg });
        }

        private async Task LogClientError(Client client, string msg)
        {
            await LogEvent($"PC{client.DeviceIndex}", new List<string> { msg });
        }

        private async Task LogUiInteaction(string description)
        {
            await LogEvent("UI", new List<string> { description });
        }

        private async void RequestStateUpdate(ThreadPoolTimer timer)
        {
            try
            {
                await SendMessageToAllParticipantsAsync(new AdminMessage { MessageType = AdminMessageType.StatusUpdateRequest });
            }
            catch (Exception) { }

            CheckClientDied();
        }

        private async Task SendMessageToAllParticipantsAsync(AdminMessage message)
        {
            await App.SessionManager.SendMessageToAllParticipantsAsync(message);

            if (message.MessageType != AdminMessageType.StatusUpdateRequest)
                await LogAdminMessage(message).ConfigureAwait(false);
        }

        private void CheckClientDied()
        {
            foreach (var client in _clients)
            {
                if (client.MessageTimeStamp != DateTimeOffset.MinValue)
                {
                    var diff = DateTimeOffset.UtcNow - client.MessageTimeStamp;
                    var didNotError = _clientStates[client.DeviceIndex - 1] != ClientState.Error;
                    if (diff.Seconds > AllowedResponseDelay && didNotError)
                    {
                        DispatcherHelper.ExecuteOnUIThreadAsync(() =>
                       {
                           client.ErrorType = ClientErrorType.ClientStoppedResponding;
                           SyncClientState(client.DeviceIndex, ClientState.Error);
                       });
                    }
                }
            }

        }

        private async void SessionManager_SessionDisconnected(object sender, Windows.System.RemoteSystems.RemoteSystemSessionDisconnectedEventArgs e)
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(() =>
            {
                foreach (var client in _clients)
                    SyncClientState(client.DeviceIndex, ClientState.Disconnected);
            });
        }

        private async void SessionManager_ParticipantLeft(object sender, ParticipantLeftEventArgs e)
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(() =>
            {
                var client = _clients.Find(c => c.HostName == e.Participant.RemoteSystem.DisplayName);
                if (client != null && _clientStates[client.DeviceIndex - 1] != ClientState.Disconnected)
                    SyncClientState(client.DeviceIndex, ClientState.Disconnected);
            });
        }

        private async void SessionManager_ParticipantJoined(object sender, ParticipantJoinedEventArgs e)
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
            {
                await SendMessageToAllParticipantsAsync(new AdminMessage { MessageType = AdminMessageType.StatusUpdateRequest });
            });
        }

        private async void DebugMessageReceived(object sender, DebugMessageEventArgs e)
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(() =>
            {
                DebugNotification?.Show(e.Message);
            });
        }

        private void SyncClientState(int deviceIndex, ClientState newState)
        {
            LogClientStateChange(deviceIndex, newState).ConfigureAwait(false);

            var currentState = _clientStates[deviceIndex - 1];

            switch (deviceIndex)
            {
                case 1:
                    PC1_State = newState;
                    break;
                case 2:
                    PC2_State = newState;
                    break;
                case 3:
                    PC3_State = newState;
                    break;
                case 4:
                    PC4_State = newState;
                    break;
            }
            _clientStates[deviceIndex - 1] = newState;

            if (currentState == newState && currentState != ClientState.Disconnected)
                return;

            StartCountdownTimer(deviceIndex, newState);
        }

        private void StartCountdownTimer(int deviceIndex, ClientState newState)
        {
            var countdown = 0;
            var errorType = ClientErrorType.None;

            switch (newState)
            {
                case ClientState.Disconnected:
                    StartConnectionTimeout(deviceIndex);
                    countdown = Constants.TIMEOUT_ADMIN_CONNECTION;
                    errorType = ClientErrorType.SessionConnectionFailed;
                    break;
                case ClientState.VR_Preparing:
                    countdown = Constants.TIMEOUT_VR_START;
                    errorType = ClientErrorType.VRLaunchFailed;
                    break;
                case ClientState.Game_Preparing:
                    countdown = Constants.TIMEOUT_GAME_START;
                    errorType = ClientErrorType.GameLaunchFailed;
                    break;
                default:
                    // abort: no need to set timer for any other states
                    StopCountdownTimer(deviceIndex);
                    return;
            }

            StopCountdownTimer(deviceIndex);

            switch (deviceIndex)
            {
                case 1:
                    _PC1_timer = CreateCountdown(countdown, 1, PC1_Timer_Text, 1, errorType);
                    break;
                case 2:
                    _PC2_timer = CreateCountdown(countdown, 1, PC2_Timer_Text, 2, errorType);
                    break;
                case 3:
                    _PC3_timer = CreateCountdown(countdown, 1, PC3_Timer_Text, 3, errorType);
                    break;
                case 4:
                    _PC4_timer = CreateCountdown(countdown, 1, PC4_Timer_Text, 4, errorType);
                    break;
            }
        }

        private void StopCountdownTimer(int deviceIndex)
        {
            switch (deviceIndex)
            {
                case 1:
                    _PC1_timer?.Cancel();
                    _PC1_timer = null;
                    break;
                case 2:
                    _PC2_timer?.Cancel();
                    _PC2_timer = null;
                    break;
                case 3:
                    _PC3_timer?.Cancel();
                    _PC3_timer = null;
                    break;
                case 4:
                    _PC4_timer?.Cancel();
                    _PC4_timer = null;
                    break;
            }
        }

        private ThreadPoolTimer CreateCountdown(int countdown, int period, TextBlock timerText, int deviceIndex, ClientErrorType errorType)
        {
            timerText.Text = countdown.ToString();
            return ThreadPoolTimer.CreatePeriodicTimer(
                _ => DispatcherHelper.ExecuteOnUIThreadAsync(() =>
                {
                    timerText.Text = (--countdown).ToString();
                    if (countdown == 0)
                    {
                        StopCountdownTimer(deviceIndex);
                        if (_clientStates[deviceIndex - 1].GetColor() == StatusColor.Yellow)
                        {

                            _clients[deviceIndex - 1].ErrorType = errorType;
                            SyncClientState(deviceIndex, ClientState.Error);
                        }
                    }
                })
            , TimeSpan.FromSeconds(period));
        }

        private void StartConnectionTimeout(int deviceIndex)
        {
            ThreadPoolTimer.CreateTimer(async _ =>
            {
                await DispatcherHelper.ExecuteOnUIThreadAsync(() =>
                {
                    var client = _clients[deviceIndex - 1];
                    if (_clientStates[deviceIndex - 1] == ClientState.Disconnected)
                    {
                        // Need to set ErrorType before syncing state so UI updates will fetch correct error type
                        client.ErrorType = ClientErrorType.SessionConnectionFailed;
                        SyncClientState(client.DeviceIndex, ClientState.Error);
                    }
                });
            }, TimeSpan.FromSeconds(Constants.TIMEOUT_ADMIN_CONNECTION));
        }

        private async void MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            await DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
            {
                object data = new Client();
                e.GetDeserializedMessage(ref data);
                var message = data as Client;
                await ProcessClientMessage(message);
            });
        }

        private async Task ProcessClientMessage(Client msg)
        {
            try
            {

                await DispatcherHelper.ExecuteOnUIThreadAsync(async () =>
                {
                    var client = _clients[msg.DeviceIndex - 1];
                    RefreshGameHostUI(client);
                    switch (msg.MessageType)
                    {
                        case ClientMessageType.Join:
                        case ClientMessageType.StateUpdate:
                            client.Clone(msg);
                            client.MessageTimeStamp = DateTimeOffset.UtcNow;
                            SaveMacAddress(client);
                            SyncLastGameStep();
                            SyncClientState(client.DeviceIndex, msg.CurrentState);
                            RefreshGameStepUI(client);
                            break;
                        case ClientMessageType.Exit:
                            SyncClientState(client.DeviceIndex, ClientState.Disconnected);
                            break;
                        case ClientMessageType.ConfigCorrupted:
                            await LogClientError(msg, $"Config file corrupted: {msg.ConfigErrorFile}");
                            await ShowClientError("Config corrupted");
                            break;
                        case ClientMessageType.ConfigMissing:
                            await LogClientError(msg, $"Config file missing: {msg.ConfigErrorFile}");
                            await ShowClientError("Config missing");
                            break;
                        case ClientMessageType.Exception:
                            await LogClientError(msg, $"App encountered exception: {msg.ExceptionMessage}");
                            await ShowClientError("Unhandled exception");
                            break;
                        case ClientMessageType.IOFailure:
                            await LogClientError(msg, $"Failed writing to file: {msg.ConfigErrorFile}");
                            await ShowClientError("Write failure");
                            break;
                    }
                    DebugNotification?.Show($"Received status update {msg.CurrentState} from PC{msg.DeviceIndex}");
                });
            }
            catch
            { }
        }
        private async Task ShowClientError(string msg)
        {
            try
            {
                var dialog = new ContentDialog
                {
                    Title = $"PC Error ({msg})",
                    PrimaryButtonText = "Ok",
                    Content = "詳細については、LOGファイルを確認してください。",
                    DefaultButton = ContentDialogButton.Primary,
                };

                await dialog.ShowAsync();
            }
            catch { }
        }
        private void RefreshGameStepUI(Client client)
        {
            var step = client.NowGameStep.ToString();
            switch (client.DeviceIndex)
            {
                case 1:
                    PC1_GameStep_Text.Text = step;
                    break;
                case 2:
                    PC2_GameStep_Text.Text = step;
                    break;
                case 3:
                    PC3_GameStep_Text.Text = step;
                    break;
                case 4:
                    PC4_GameStep_Text.Text = step;
                    break;
            }
        }

        private void RefreshGameHostUI(Client client)
        {
            DispatcherHelper.ExecuteOnUIThreadAsync(() =>
            {

                switch (client.DeviceIndex)
                {
                    case 1:
                        PC1_GameHostIcon.Visibility = _clients[0].IsGameHost ? Visibility.Visible : Visibility.Collapsed;
                        break;
                    case 2:
                        PC2_GameHostIcon.Visibility = _clients[1].IsGameHost ? Visibility.Visible : Visibility.Collapsed;
                        break;
                    case 3:
                        PC3_GameHostIcon.Visibility = _clients[2].IsGameHost ? Visibility.Visible : Visibility.Collapsed;
                        break;
                    case 4:
                        PC4_GameHostIcon.Visibility = _clients[3].IsGameHost ? Visibility.Visible : Visibility.Collapsed;
                        break;
                }
            });
        }
        private void SyncLastGameStep()
        {
            var nowSteps = _clients.Select(c => c.NowGameStep).Where(step => step > 0);
            if (nowSteps.Any())
                if (nowSteps.Min() == 8)
                    EarliestGameStepReported = 0;
                else
                    EarliestGameStepReported = nowSteps.Min();
        }

        private void SaveMacAddress(Client client)
        {
            if (string.IsNullOrEmpty(client.MacAddress))
                return;
            var localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values[$"MAC_{client.DeviceIndex}"] = client.MacAddress;
        }

        private void UI_Main_BackButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowBusy(0.5);
            UI_Main.Visibility = Visibility.Collapsed;
            UI_Settings.Visibility = Visibility.Visible;
        }

        private async void UI_Main_CloseButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowBusy(0.5);
            await LogEvent("UI", new List<string> { "=============================== App Closed ===============================" });
            App.Current.Exit();
        }

        private void UI_Settings_BackButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowBusy(0.5);
            UI_Settings.Visibility = Visibility.Collapsed;
            UI_Main.Visibility = Visibility.Visible;
        }

        private async Task<bool> Confirm()
        {
            var dialog = new ContentDialog
            {
                Title = "実行します。よろしいですか？",
                PrimaryButtonText = "Ok",
                Content = "対応を確認してください",
                DefaultButton = ContentDialogButton.Primary,
                CloseButtonText = "Cancel"
            };

            var result = await dialog.ShowAsync();
            return result == ContentDialogResult.Primary;
        }

        private async Task<bool> ConfirmIfAnyErrorState()
        {
            // in case no client is in error state, return true to continue action
            var anyError = _clientStates.Any(s => s == ClientState.Error);
            if (!anyError)
            {
                ShowBusy();
                return true;
            }

            var dialog = new ContentDialog
            {
                Title = "実行します。よろしいですか？",
                Content = "一台もしくは複数のPCに不具合があり",
                PrimaryButtonText = "Ok",
                DefaultButton = ContentDialogButton.Primary,
                CloseButtonText = "Cancel"
            };

            var result = await dialog.ShowAsync();
            return result == ContentDialogResult.Primary;
        }

        private async void Open_Taboo_Button_Click(object sender, RoutedEventArgs e)
        {
            var confirmed = await ConfirmIfAnyErrorState();

            if (!confirmed)
                return;

            await LogUiInteaction("Open Taboo Button Clicked");

            var gameHost = FindGameHostCandidate();
            if (gameHost == null)
                return;

            gameHost.IsGameHost = true;
            RefreshGameHostUI(gameHost);

            var msg = new AdminMessage
            {
                MessageType = AdminMessageType.Taboo_Launch,
                GameHostIndex = gameHost.DeviceIndex,
                PC1Language = _clients[0].DisplayLanguage,
                PC2Language = _clients[1].DisplayLanguage,
                PC3Language = _clients[2].DisplayLanguage,
                PC4Language = _clients[3].DisplayLanguage,
            };
            await SendMessageToAllParticipantsAsync(msg);

            if (_clients.Any(c => c.DisplayLanguage == DisplayLanguage.NONE))
                ShowBusy(3);
        }

        private async void Close_Taboo_Button_Click(object sender, RoutedEventArgs e)
        {
            _chosenCloseTabooPCs = new bool[4];

            CloseTabooAllPCs.IsChecked = false;
            var result = await ChooseTabooClosePCs_Dialog.ShowAsync();
            if (result != ContentDialogResult.Primary || _chosenCloseTabooPCs.All(b => !b))
                return;

            await LogUiInteaction("Close Taboo Button Clicked");

            // find next host before closing taboo in case host is one of pcs 
            var newHost = FindRestartHost();
            Client oldHost = null;

            for (int i = 0; i < _chosenCloseTabooPCs.Length; i++)
            {
                if (_chosenCloseTabooPCs[i])
                {
                    var client = _clients[i];
                    var isHost = client.IsGameHost;
                    client.IsGameHost = false;
                    RefreshGameHostUI(client);

                    // skip sending message, we will close host's taboo last
                    if (isHost)
                    {
                        oldHost = client;
                        continue;
                    }

                    var msg = new AdminMessage() { MessageType = AdminMessageType.Taboo_Shutdown, CloseTabooDeviceIndex = i + 1 };
                    await SendMessageToAllParticipantsAsync(msg);
                }
            }

            // close old host's taboo
            if (oldHost != null)
                await SendMessageToAllParticipantsAsync(
                    new AdminMessage() { MessageType = AdminMessageType.Taboo_Shutdown, CloseTabooDeviceIndex = oldHost.DeviceIndex });

            if (newHost != null)
                await SendMessageToAllParticipantsAsync(
                    new AdminMessage { MessageType = AdminMessageType.Taboo_RestartClose, GameHostIndex = newHost.DeviceIndex });
        }

        private Client FindRestartHost()
        {
            var candidates = new List<Client>();
            for (int i = 0; i < _chosenCloseTabooPCs.Length; i++)
            {
                if (_chosenCloseTabooPCs[i] == false)
                    candidates.Add(_clients[i]);
            }

            return candidates.FirstOrDefault(c => c.CurrentState == ClientState.Game_Ready);
        }

        private async void Start_Taboo_Button_Click(object sender, RoutedEventArgs e)
        {
            var confirmed = await ConfirmIfAnyErrorState();

            if (!confirmed)
                return;

            await LogUiInteaction("Start Taboo Button Clicked");

            int chosenGameStep = 1;
            bool clickedCancel = false;

            if (EarliestGameStepReported > 0)
            {
                ChooseScene_Continue.IsChecked = true;
                DisplayedGameSceneIndex = EarliestGameStepReported - 1;
                var result = await ChooseSceneDialog.ShowAsync();

                clickedCancel = result != ContentDialogResult.Primary;
                if (clickedCancel)
                    return;

                if (ChooseScene_Continue.IsChecked.Value)
                    chosenGameStep = ChooseScene_Scene.SelectedIndex + 1;

                EarliestGameStepReported = 0;
            }

            var msg = new AdminMessage()
            {
                MessageType = AdminMessageType.Taboo_Play,
                GameStep = chosenGameStep,
                PC1Language = _clients[0].DisplayLanguage,
                PC2Language = _clients[1].DisplayLanguage,
                PC3Language = _clients[2].DisplayLanguage,
                PC4Language = _clients[3].DisplayLanguage,
            };
            await SendMessageToAllParticipantsAsync(msg);
        }

        private async void Restart_Taboo_Button_Click(object sender, RoutedEventArgs e)
        {
            var confirmed = await Confirm();

            if (!confirmed)
                return;

            await LogUiInteaction("Restart Taboo Button Clicked");

            var msg = new AdminMessage { MessageType = AdminMessageType.Taboo_Restart };
            await SendMessageToAllParticipantsAsync(msg);
        }

        private async void Fix_Button_Click(object sender, RoutedEventArgs e)
        {
            var markdown = "| Device | Error Type | アクション |\n";
            markdown += "|:-:|:-:|:-:|\n";
            var action = "";
            var error = "";
            var stoppedResponding = "**__エラーがないかPCを確認してください。__**";
            var wol = "**__リモート電源オン（Wake-on-Lan）__**";
            var restart2 = "VRソフトウェアを再起動";
            for (int i = 0; i < _clientStates.Count; i++)
            {
                if (_clientStates[i] == ClientState.Error)
                {
                    switch (_clients[i].ErrorType)
                    {
                        case ClientErrorType.SessionConnectionFailed:
                            error = "接続失敗";
                            action = wol;
                            break;
                        case ClientErrorType.VRLaunchFailed:
                            error = "VRソフトウェア起動失敗";
                            action = restart2;
                            break;
                        case ClientErrorType.GameLaunchFailed:
                            error = "ゲーム起動失敗";
                            action = restart2;
                            break;
                        case ClientErrorType.TabooSceneOutOfSync:
                            error = "ゲームの同期エラー";
                            action = restart2;
                            break;
                        case ClientErrorType.StarVR_Malfunctioned:
                            error = "ヘッドセットの不具合";
                            action = restart2;
                            break;
                        case ClientErrorType.StarVR_Frozen:
                            error = "Failed to restart StarVR";
                            action = restart2;
                            break;
                        case ClientErrorType.ClientStoppedResponding:
                            error = "PCの応答がありません";
                            action = stoppedResponding;
                            break;
                    }
                    markdown += $"| PC{i + 1} | {error} | {action.ToString()} |\n";
                }
            }

            FixMarkdown.Text = markdown;
            var result = await FixDialog.ShowAsync();

            if (result == ContentDialogResult.Primary)
            {
                var msg = new AdminMessage { MessageType = AdminMessageType.AttemptFix };
                await SendMessageToAllParticipantsAsync(msg);

                // do wol if error is connection timeout
                if (markdown.Contains(wol))
                    WakeAllOnLan();
            }
            await LogUiInteaction("Fix Button Clicked");
        }

        private bool _OpeningVideoToggle = false;
        private async void Opening_Video_Button_Click(object sender, RoutedEventArgs e)
        {
            ShowBusy();

            await LogUiInteaction("Opening Video Play Button Clicked");

            _OpeningVideoToggle = !_OpeningVideoToggle;
            Opening_Video_Toggle_Image();

            if (_OpeningVideoToggle && _TrailerVideoToggle)
            {
                _TrailerVideoToggle = false;
                Trailer_Video_Toggle_Image();
            }

            var msg = new AdminMessage
            {
                MessageType = _OpeningVideoToggle ? AdminMessageType.Video_Opening_Open : AdminMessageType.Video_Opening_Close,
                VideoPlayIndex = _videoDeviceIndex
            };
            await SendMessageToAllParticipantsAsync(msg).ConfigureAwait(false);
        }
        private void Opening_Video_Toggle_Image()
        {
            Opening_Video_Image.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Second_{(_OpeningVideoToggle ? "Blue" : "Black")}_Video_01.png"));
        }

        private bool _TrailerVideoToggle = false;
        private async void Trailer_Video_Button_Click(object sender, RoutedEventArgs e)
        {
            ShowBusy();

            await LogUiInteaction("Trailer Video Play Button Clicked");

            _TrailerVideoToggle = !_TrailerVideoToggle;
            Trailer_Video_Toggle_Image();

            if (_TrailerVideoToggle && _OpeningVideoToggle)
            {
                _OpeningVideoToggle = false;
                Opening_Video_Toggle_Image();
            }

            var msg = new AdminMessage
            {
                MessageType = _TrailerVideoToggle ? AdminMessageType.Video_Trailer_Open : AdminMessageType.Video_Trailer_Close,
                VideoPlayIndex = _videoDeviceIndex
            };
            await SendMessageToAllParticipantsAsync(msg).ConfigureAwait(false);
        }

        private void Trailer_Video_Toggle_Image()
        {
            Trailer_Video_Image.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Second_{(_TrailerVideoToggle ? "Blue" : "Black")}_Video_02.png"));
        }

        private async void Choose_PC_VideoLocation_Button_Click(object sender, RoutedEventArgs e)
        {
            ShowBusy();

            await LogUiInteaction("Choose PC Video Location Button Clicked");

            var button = (Button)sender;
            _videoDeviceIndex = Int32.Parse(button.Tag.ToString());
        }

        private bool _ChatToggle = false;
        private async void Chat_Button_Click(object sender, RoutedEventArgs e)
        {
            ShowBusy();

            await LogUiInteaction("Chat Button Clicked");

            _ChatToggle = !_ChatToggle;
            Chat_Button_Image.Source = new BitmapImage(new Uri($"ms-appx:///Assets/Second_Button_Chat_{(_ChatToggle ? "Blue" : "Black")}.png"));

            var msg = new AdminMessage
            {
                MessageType = _ChatToggle ? AdminMessageType.Chat_Open : AdminMessageType.Chat_Close,
                VideoPlayIndex = _videoDeviceIndex,
                ChatHostIP = _clients[_videoDeviceIndex - 1].IPAddress,
            };
            await SendMessageToAllParticipantsAsync(msg).ConfigureAwait(false);
        }

        private async void PowerOffPCs_Button_Click(object sender, RoutedEventArgs e)
        {
            _chosenPowerOffPCs = new bool[4];

            PowerOffAllPCs.IsChecked = false;
            var result = await ChoosePowerOffPCs_Dialog.ShowAsync();
            if (result != ContentDialogResult.Primary || _chosenPowerOffPCs.All(b => !b))
                return;

            await LogUiInteaction("Power Off PCs Button Clicked");

            PowerOffPCs_Image.Source = new BitmapImage(new Uri("ms-appx:///Assets/Second_Button_Close_Blue.png"));

            for (int i = 0; i < _chosenPowerOffPCs.Length; i++)
            {
                if (_chosenPowerOffPCs[i])
                {
                    var msg = new AdminMessage() { MessageType = AdminMessageType.ShutdownPC, ShutDownDeviceIndex = i + 1 };
                    await SendMessageToAllParticipantsAsync(msg);
                }
            }
            await Task.Delay(5 * 1000);

            PowerOffPCs_Image.Source = new BitmapImage(new Uri("ms-appx:///Assets/Second_Button_Close_Black.png"));
        }

        private async void Open_PC_Button_Click(object sender, RoutedEventArgs e)
        {
            ShowBusy();

            await LogUiInteaction("Open PCs Button Clicked");

            foreach (var client in _clients)
                SyncClientState(client.DeviceIndex, _clientStates[client.DeviceIndex - 1]);

            WakeAllOnLan();

            Open_PC_Image.Source = new BitmapImage(new Uri("ms-appx:///Assets/Second_Button_Open_Blue.png"));
            await Task.Delay(5 * 1000);
            Open_PC_Image.Source = new BitmapImage(new Uri("ms-appx:///Assets/Second_Button_Open_Black.png"));
        }

        private void WakeAllOnLan()
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            object pc1 = localSettings.Values["MAC_1"];
            object pc2 = localSettings.Values["MAC_2"];
            object pc3 = localSettings.Values["MAC_3"];
            object pc4 = localSettings.Values["MAC_4"];

            if (pc1 != null)
                WakeOnLan((string)pc1);
            if (pc2 != null)
                WakeOnLan((string)pc2);
            if (pc3 != null)
                WakeOnLan((string)pc3);
            if (pc4 != null)
                WakeOnLan((string)pc4);
        }

        private async void WakeOnLan(string mac)
        {
            await Utils.WakeOnLan(mac);
        }

        private async void DisplayLanguage_SelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            var combo = (ComboBox)sender;
            var pcIndex = int.Parse(combo.Tag.ToString());
            DisplayLanguage lang = DisplayLanguage.JA_JP;

            var langCode = (combo.SelectedItem as ComboBoxItem).Content;
            switch (langCode)
            {
                case "中国語":
                    lang = DisplayLanguage.ZH_CN;
                    break;
                case "日本語":
                    lang = DisplayLanguage.JA_JP;
                    break;
                case "韓国語":
                    lang = DisplayLanguage.KO_KR;
                    break;
                case "英語":
                    lang = DisplayLanguage.EN_US;
                    break;
                case "None":
                    lang = DisplayLanguage.NONE;
                    break;
            }

            // don't allow changing language for game host
            //var client = _clients[pcIndex - 1];
            //if (client.IsGameHost && lang == DisplayLanguage.NONE)
            //{
            //    await ShowCannotChangeServerLanguage(pcIndex);
            //    switch (client.DisplayLanguage)
            //    {
            //        case DisplayLanguage.JA_JP:
            //            combo.SelectedIndex = 0;
            //            break;
            //        case DisplayLanguage.KO_KR:
            //            combo.SelectedIndex = 1;
            //            break;
            //        case DisplayLanguage.ZH_CN:
            //            combo.SelectedIndex = 2;
            //            break;
            //        case DisplayLanguage.EN_US:
            //            combo.SelectedIndex = 3;
            //            break;
            //    }
            //    return;
            //}
            //else
            _clients[pcIndex - 1].DisplayLanguage = lang;
        }

        //private async Task ShowCannotChangeServerLanguage(int pcIndex)
        //{
        //    var dialog = new ContentDialog
        //    {
        //        Title = "Cannot change language for Game Server",
        //        PrimaryButtonText = "Ok",
        //        Content = $"Please close the game first if you want to set this PC's language to None",
        //        DefaultButton = ContentDialogButton.Primary,
        //    };

        //    await dialog.ShowAsync();
        //}

        private async void Debug_Button_Click(object sender, RoutedEventArgs e)
        {
            await DebugDialog.ShowAsync();
        }

        private void DebugDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            for (int i = 0; i < _clientStates.Count; i++)
            {
                foreach (var state in Enum.GetValues(typeof(ClientState)).Cast<ClientState>())
                {
                    var name = $"PC{i + 1}_{state}";
                    var radio = (FindName(name) as RadioButton);
                    if (radio != null && radio.IsChecked.Value)
                        if (_clientStates[i] != state)
                        {
                            SyncClientState(i + 1, state);
                            break;
                        }
                }
            }
        }

        private void DebugDialog_Opened(ContentDialog sender, ContentDialogOpenedEventArgs args)
        {
            for (int i = 0; i < _clientStates.Count; i++)
            {
                var name = $"PC{i + 1}_{_clientStates[i]}";

                if (FindName(name) is RadioButton radio)
                    radio.IsChecked = true;
            }
        }

        private void ShowBusy(double seconds = 1)
        {
            IsBusy = true;
            ThreadPoolTimer.CreateTimer(_ => DispatcherHelper.ExecuteOnUIThreadAsync(() => IsBusy = false), TimeSpan.FromSeconds(seconds));
        }

        private void PowerOffAllPCs_Checked(object sender, RoutedEventArgs e)
        {
            PC1_PowerOff.IsChecked = PC2_PowerOff.IsChecked = PC3_PowerOff.IsChecked = PC4_PowerOff.IsChecked = true;
        }

        private void PowerOffAllPCs_Unchecked(object sender, RoutedEventArgs e)
        {
            PC1_PowerOff.IsChecked = PC2_PowerOff.IsChecked = PC3_PowerOff.IsChecked = PC4_PowerOff.IsChecked = false;
        }

        private void PowerOffAllPCs_Indeterminate(object sender, RoutedEventArgs e)
        {
            if (PC1_PowerOff.IsChecked == true && PC2_PowerOff.IsChecked == true &&
                PC3_PowerOff.IsChecked == true && PC4_PowerOff.IsChecked == true)
                PowerOffAllPCs.IsChecked = false;
        }

        private bool[] _chosenPowerOffPCs = new bool[4];
        private void ChoosePowerOffPCs_Dialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            _chosenPowerOffPCs[0] = PC1_PowerOff.IsChecked == true;
            _chosenPowerOffPCs[1] = PC2_PowerOff.IsChecked == true;
            _chosenPowerOffPCs[2] = PC3_PowerOff.IsChecked == true;
            _chosenPowerOffPCs[3] = PC4_PowerOff.IsChecked == true;
        }

        private void PC_PowerOff_Checked(object sender, RoutedEventArgs e)
        {
            SetPowerOffCheckedState();
        }

        private void PC_PowerOff_Unchecked(object sender, RoutedEventArgs e)
        {
            SetPowerOffCheckedState();
        }

        private void SetPowerOffCheckedState()
        {
            // Controls are null the first time this is called, so we just
            // need to perform a null check on any one of the controls.
            if (PC1_PowerOff != null)
            {
                if (PC1_PowerOff.IsChecked == true && PC2_PowerOff.IsChecked == true &&
                    PC3_PowerOff.IsChecked == true && PC4_PowerOff.IsChecked == true)
                {
                    PowerOffAllPCs.IsChecked = true;
                }
                else if (PC1_PowerOff.IsChecked == false && PC2_PowerOff.IsChecked == false &&
                         PC3_PowerOff.IsChecked == false && PC4_PowerOff.IsChecked == false)
                {
                    PowerOffAllPCs.IsChecked = false;
                }
                else
                    // Set third state (indeterminate) by setting IsChecked to null.
                    PowerOffAllPCs.IsChecked = null;
            }
        }

        private void CloseTabooAllPCs_Checked(object sender, RoutedEventArgs e)
        {
            PC1_CloseTaboo.IsChecked = PC2_CloseTaboo.IsChecked = PC3_CloseTaboo.IsChecked = PC4_CloseTaboo.IsChecked = true;
        }

        private void CloseTabooAllPCs_Unchecked(object sender, RoutedEventArgs e)
        {
            PC1_CloseTaboo.IsChecked = PC2_CloseTaboo.IsChecked = PC3_CloseTaboo.IsChecked = PC4_CloseTaboo.IsChecked = false;
        }

        private void CloseTabooAllPCs_Indeterminate(object sender, RoutedEventArgs e)
        {
            if (PC1_CloseTaboo.IsChecked == true && PC2_CloseTaboo.IsChecked == true &&
                PC3_CloseTaboo.IsChecked == true && PC4_CloseTaboo.IsChecked == true)
                CloseTabooAllPCs.IsChecked = false;
        }

        private void PC_CloseTaboo_Checked(object sender, RoutedEventArgs e) => SetCloseTabooState();
        private void PC_CloseTaboo_Unchecked(object sender, RoutedEventArgs e) => SetCloseTabooState();

        private void SetCloseTabooState()
        {
            // Controls are null the first time this is called, so we just
            // need to perform a null check on any one of the controls.
            if (PC1_CloseTaboo != null)
            {
                if (PC1_CloseTaboo.IsChecked == true && PC2_CloseTaboo.IsChecked == true &&
                    PC3_CloseTaboo.IsChecked == true && PC4_CloseTaboo.IsChecked == true)
                {
                    CloseTabooAllPCs.IsChecked = true;
                }
                else if (PC1_CloseTaboo.IsChecked == false && PC2_CloseTaboo.IsChecked == false &&
                         PC3_CloseTaboo.IsChecked == false && PC4_CloseTaboo.IsChecked == false)
                {
                    CloseTabooAllPCs.IsChecked = false;
                }
                else
                    // Set third state (indeterminate) by setting IsChecked to null.
                    CloseTabooAllPCs.IsChecked = null;
            }
        }

        private bool[] _chosenCloseTabooPCs = new bool[4];
        private void ChooseTabooClosePCs_Dialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            _chosenCloseTabooPCs[0] = PC1_CloseTaboo.IsChecked == true;
            _chosenCloseTabooPCs[1] = PC2_CloseTaboo.IsChecked == true;
            _chosenCloseTabooPCs[2] = PC3_CloseTaboo.IsChecked == true;
            _chosenCloseTabooPCs[3] = PC4_CloseTaboo.IsChecked == true;
        }

        private void ChooseTabooClosePCs_Dialog_Opened(ContentDialog sender, ContentDialogOpenedEventArgs args)
        {
            CloseTabooAllPCs.IsChecked = true;
        }
    }
}
