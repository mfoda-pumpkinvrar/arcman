﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Net;
using System.Threading;

using ArcMan;

using PMonitor.Core;

using Windows.Foundation.Collections;
using Windows.Storage;

namespace FullTrustLauncher
{
    class Program
    {
        private static readonly IPropertySet settings = ApplicationData.Current.LocalSettings.Values;

        static void Main(string[] args)
        {
            if (args.Length > 2)
            {
                switch (args[2])
                {
                    case "/OpenTaboo":
                        OpenTaboo();
                        break;
                    case "/CloseTaboo":
                        KillTaboo();
                        break;
                    case "/OpenSteamVR":
                        OpenProcess(Constants.STEAMVR_PROCESS_NAME, Constants.STEAMVR_EXE_PATH);
                        break;
                    case "/CloseSteamVR":
                        KillProcess(Constants.STEAMVR_PROCESS_NAME);
                        break;
                    case "/OpenStarVR":
                        OpenProcess(Constants.STARVR_PROCESS_NAME, Constants.STARVR_EXE_PATH);
                        break;
                    case "/CloseStarVR":
                        KillStarVR();
                        break;
                    case "/StartProcessMonitor":
                        StartProcessMonitor();
                        break;
                    case "/Cleanup":
                        KillAllInstancesOfSelf();
                        break;
                    case "/BringToFront":
                        BringToFront();
                        break;
                    case "/Shutdown":
                        Shutdown();
                        break;
                    case "/ChatOpenClient":
                        ChatOpenClient();
                        break;
                    case "/ChatOpenHost":
                        ChatOpenHost();
                        break;
                    case "/ChatClose":
                        ChatClose();
                        break;
                    case "/CloseVLC":
                        CloseVLC();
                        break;
                    case "/OpeningVideoOpen":
                        OpeningVideoOpen();
                        break;
                    case "/TrailerVideoOpen":
                        TrailerVideoOpen();
                        break;
                }
            }
        }

        private static void KillStarVR()
        {
            KillProcess(Constants.STARVR_PROCESS_NAME);

            var psi = new ProcessStartInfo("cmd.exe", "/C Taskkill /F /IM StarVRSystem.exe")
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = true
            };
            Process.Start(psi);
        }

        private static void KillAllInstancesOfSelf()
        {
            var self_id = Process.GetCurrentProcess().Id;
            foreach (var p in Process.GetProcessesByName("FullTrustLauncher"))
                if (p.Id != self_id)
                    p.Kill();
        }

        private static void KillTaboo()
        {
            var taboo_processes = Process.GetProcessesByName(Constants.TABOO_PROCESS_NAME);
            foreach (var p in taboo_processes)
                KillProcessAndChildren(p.Id);
        }

        private static void TrailerVideoOpen()
        {
            var psi = new ProcessStartInfo("cmd.exe", $"/C vlc --fullscreen --play-and-exit {Constants.TABOO_TRAILER_VIDEO_PATH}")
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = true
            };
            Process.Start(psi);
        }

        private static void OpeningVideoOpen()
        {
            var psi = new ProcessStartInfo("cmd.exe", $"/C vlc --fullscreen --play-and-exit {Constants.TABOO_OPENING_VIDEO_PATH}")
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = true
            };
            Process.Start(psi);
        }

        private static void CloseVLC()
        {
            var psi = new ProcessStartInfo("cmd.exe", "/C Taskkill /F /IM vlc.exe")
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = true
            };
            Process.Start(psi);
        }

        private static void ChatClose()
        {
            var psi = new ProcessStartInfo("cmd.exe", "/C taskkill /s localhost /IM mumble.exe")
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = true
            };
            Process.Start(psi);
        }

        private static void ChatOpenHost()
        {

        }

        private static void ChatOpenClient()
        {
            object hostIP = settings["MumbleHostIP"];
            if (hostIP != null)
            {
                var ip = (string)hostIP;
                var psi = new ProcessStartInfo("cmd.exe", $"/C start /min mumble mumble://{Dns.GetHostName()}@{hostIP}")
                {
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    UseShellExecute = true
                };
                Process.Start(psi);
            }
        }

        private static void Shutdown()
        {
            var psi = new ProcessStartInfo("shutdown", "/s /t 0")
            {
                CreateNoWindow = true,
                UseShellExecute = false
            };
            Process.Start(psi);
        }

        private static void BringToFront()
        {

            //var uri = new Uri("pumpkin-arcman:");
            //Windows.System.Launcher.LaunchUriAsync(uri).GetResults();
            var psi = new ProcessStartInfo
            {
                UseShellExecute = true,
                WindowStyle = ProcessWindowStyle.Minimized,
                FileName = "pumpkin-arcman:",
            };
            Process.Start(psi);
        }

        private static void ModifySettings(bool signalDataChanged, string k, bool v)
        {
            settings[k] = v;
            if (signalDataChanged)
                ApplicationData.Current.SignalDataChanged();
        }
        private static void ModifySettings(bool signalDataChanged, string k, string v)
        {
            settings[k] = v;
            if (signalDataChanged)
                ApplicationData.Current.SignalDataChanged();
        }
        private static void ModifySettings(bool signalDataChanged, string k, int v)
        {
            settings[k] = v;
            if (signalDataChanged)
                ApplicationData.Current.SignalDataChanged();
        }

        private static ProcessState StartProcessMonitor()
        {
            var steamvrP = ProcessState.NotRunning;
            var starvrP = ProcessState.NotRunning;
            var tabooP = ProcessState.NotRunning;
            var monitor = ProcessMonitorFactory.BuildDefaultOSProcessMonitor();
            var CHECK_INTERVAL = 300;

            // Reset values from previous run
            ModifySettings(false, "SteamVR_Running", false);
            ModifySettings(false, "StarVR_Running", false);
            ModifySettings(false, "Taboo_Running", false);

            while (true)
            {
                // pmonitor will crash if more than one matching process is found for any given process name
                try
                {
                    monitor.RefreshInformation();
                    var processInfo = monitor.GetProcessInformation();

                    foreach (var p in processInfo)
                    {
                        var newState = p.State;
                        switch (p.FriendlyName)
                        {
                            case Constants.STEAMVR_PROCESS_NAME:
                                if (steamvrP != newState)
                                {
                                    steamvrP = newState;
                                    ModifySettings(true, "SteamVR_Running", steamvrP == ProcessState.Running);
                                }
                                break;
                            case Constants.STARVR_PROCESS_NAME:
                                if (starvrP != newState)
                                {
                                    starvrP = newState;
                                    ModifySettings(true, "StarVR_Running", starvrP == ProcessState.Running);
                                }
                                break;
                            case Constants.TABOO_PROCESS_NAME:
                                if (tabooP != newState)
                                {
                                    tabooP = newState;
                                    ModifySettings(true, "Taboo_Running", tabooP == ProcessState.Running);
                                }
                                break;
                        }
                    }
                }

                catch (Exception)
                {
                    var taboo_crash = Process.GetProcesses().Where(proc => proc.MainWindowTitle == "Error" && proc.ProcessName == Constants.TABOO_PROCESS_NAME);
                    if (taboo_crash.Any())
                        KillTaboo();
                }
                Thread.Sleep(CHECK_INTERVAL);
            }
        }

        private static bool IsProcessRunning(string name) => Process.GetProcessesByName(name).Length > 0;

        private static void OpenTaboo()
        {
            if (IsProcessRunning(Constants.TABOO_PROCESS_NAME))
                return;

            var info = new ProcessStartInfo(Constants.TABOO_EXE_PATH)
            {
                UseShellExecute = false,
                WindowStyle = ProcessWindowStyle.Maximized
            };
            Process.Start(info);
        }
        private static void OpenProcess(string pName, string pPath)
        {
            if (IsProcessRunning(pName))
                return;

            var info = new ProcessStartInfo(pPath);
            Process.Start(info);
        }
        private static void KillProcess(string pName)
        {
            var processes = Process.GetProcessesByName(pName);
            foreach (var p in processes)
                p.Kill();
        }

        // Better kill method for Taboo since it has child processes
        private static void KillProcessAndChildren(int pid)
        {
            ManagementObjectSearcher processSearcher = new ManagementObjectSearcher
              ("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection processCollection = processSearcher.Get();

            // We must kill child processes first!
            if (processCollection != null)
            {
                foreach (ManagementObject mo in processCollection)
                {
                    KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"])); //kill child processes(also kills childrens of childrens etc.)
                }
            }
            // Then kill parents.
            try
            {
                Process proc = Process.GetProcessById(pid);
                if (!proc.HasExited) proc.Kill();
            }
            catch (ArgumentException)
            {
                // Process already exited.
            }
        }
    }
}
